"""
Tomar una secuencia nucleotidica
traducirla a secuencia aminoacidica

A partir de la secuencia aminoacidica retrotraducir todas las secuencias
nucleotidicas que se pueden sacar a partir de esa secuencia

alinear todas esas secuencias nucleotidicas con la secuencia nucleotidica
principal (se puede hacer con blast de manera local)


la que tenga un menor porcentaje de identidad sera la secuencia nucleotidica
nas modificada de la secuencia original pero que aun asi secuencie para la misma
proteina

FUNCIONA PERO ES BASTANTE LENTO CUANDO SON MUCHAS SECUENCIAS
VER COMO OPTIMIZAR EN ESOS TEMAS PARA QUE NO DEMORE 100000 AÑOS CON UNA SECUENCIA NORMAL

CONSIDERAR QUE CUANDO SE TRADUCE UN NUCLEOTIDO SE PASA A 6 MARCOS DE LECTURA
EL MARCO DE LECTURA MAS GRANDE ES EL MARCO DE LECTURA ABIERTO Y ES EL QUE EFECTIVAMENTE
SE TERMINARA PASANDO A PROTEINA

SE PUEDE INTENTAR TRADUCIR PRIMERO LA SECUENCIA A LOS 6 MARCOS DE LECTURA, AGARRAR EL AMINOACIDO QUE EFECTIVAMENTE
ES QUE EL TRADUCE (QUE DEBERIA SER MAS PEQUEÑO)
ACHICAR LAS SECUENCIAS PUEDE SER UTIL AL MOMENTO DE TRADUCIR
"""


from pysam import FastaFile
from itertools import product
import os

code = {
    "A": ["GCT", "GCC", "GCA", "GCG"],
    "R": ["CGT", "CGC", "CGA", "CGG", "AGA", "AGG"],
    "N": ["AAT", "AAC"],
    "D": ["GAT", "GAC"],
    "C": ["TGT", "TGC"],
    "Q": ["CAA", "CAG"],
    "E": ["GAA", "GAG"],
    "G": ["GGT", "GGC", "GGA", "GGG"],
    "H": ["CAT", "CAC"],
    "I": ["ATT", "ATC", "ATA"],
    "L": ["TTA", "TTG", "CTT", "CTC", "CTA", "CTG"],
    "K": ["AAA", "AAG"],
    "M": ["ATG"],
    "F": ["TTT", "TTC"],
    "P": ["CCT", "CCC", "CCA", "CCG"],
    "S": ["TCT", "TCC", "TCA", "TCG", "AGT", "AGC"],
    "T": ["ACT", "ACC", "ACA", "ACG"],
    "W": ["TGG"],
    "Y": ["TAT", "TAC"],
    "V": ["GTT", "GTC", "GTA", "GTG"],
    "X": ["TAA", "TAG", "TGA"]  # Stop codons
}

codon_table = {
    'TTT': 'F', 'TTC': 'F', 'TTA': 'L', 'TTG': 'L',
    'TCT': 'S', 'TCC': 'S', 'TCA': 'S', 'TCG': 'S',
    'TAT': 'Y', 'TAC': 'Y', 'TAA': 'X', 'TAG': 'X',
    'TGT': 'C', 'TGC': 'C', 'TGA': 'X', 'TGG': 'W',
    'CTT': 'L', 'CTC': 'L', 'CTA': 'L', 'CTG': 'L',
    'CCT': 'P', 'CCC': 'P', 'CCA': 'P', 'CCG': 'P',
    'CAT': 'H', 'CAC': 'H', 'CAA': 'Q', 'CAG': 'Q',
    'CGT': 'R', 'CGC': 'R', 'CGA': 'R', 'CGG': 'R',
    'ATT': 'I', 'ATC': 'I', 'ATA': 'I', 'ATG': 'M',
    'ACT': 'T', 'ACC': 'T', 'ACA': 'T', 'ACG': 'T',
    'AAT': 'N', 'AAC': 'N', 'AAA': 'K', 'AAG': 'K',
    'AGT': 'S', 'AGC': 'S', 'AGA': 'R', 'AGG': 'R',
    'GTT': 'V', 'GTC': 'V', 'GTA': 'V', 'GTG': 'V',
    'GCT': 'A', 'GCC': 'A', 'GCA': 'A', 'GCG': 'A',
    'GAT': 'D', 'GAC': 'D', 'GAA': 'E', 'GAG': 'E',
    'GGT': 'G', 'GGC': 'G', 'GGA': 'G', 'GGG': 'G'
}


# Traduce de nucleotido a aminoacido
def translate(seq):
    protein = ""
    codon = ""
    for i in range(len(seq)):
        codon += seq[i]
        if len(codon) == 3:
            protein += codon_table[codon]
            codon = ""
    return protein


def combine(sequence, original):
    total = len(sequence)
    posibilities = []
    for i in range(total):
        posibilities.append(code[sequence[i]])
    # Generar todas las combinaciones posibles
    fseq = ""
    # se puede sacar identidad directamente aqui
    min = 100
    for seq in product(*posibilities):
        for codon in seq:
            fseq += codon
        identity = get_identity(original, fseq)
        fseq = ""
        if identity < min:
            min = identity
            print(min)
    return min


# Obtiene una secuencia de un fasta
def get_seq(filename, seqname):
    tmp = FastaFile(filename)
    os.remove("sequence.fasta.fai")
    return tmp.fetch(seqname)


# Calcular el porcentaje de identidad entre dos secuencias
def get_identity(seq1, seq2):
    if len(seq1) != len(seq2):
        return
    matches = 0
    for i in range(len(seq1)):
        if seq1[i] == seq2[i]:
            matches += 1
    id_percent = (matches/len(seq1)) * 100
    return id_percent


# Cantidad de combinaciones
def comb_amount(amino_seq):
    total = 1
    for aa in amino_seq:
        total *= len(code[aa])
    return total


def main():

    nuc_seq = get_seq("sequence.fasta", "NM_005318.4")
    amino_seq = translate(nuc_seq)
    print(f"secuencia nucleotidica inicial: {nuc_seq}")
    print(f"secuencia aminoacidica: {amino_seq}")
    print(f"Numero de combinaciones: {comb_amount(amino_seq)}")
    print(f"Maximo porcentaje de cambio: {100- combine(amino_seq, nuc_seq)}")
    # se podria hacer en otro orden
    # secuencia - alinear - guardar el % de identidad - seguir

    
if __name__ == "__main__":
    main()
