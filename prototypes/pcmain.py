# version con productor-consumidor
"""
Tomar una secuencia nucleotidica
traducirla a secuencia aminoacidica

A partir de la secuencia aminoacidica retrotraducir todas las secuencias
nucleotidicas que se pueden sacar a partir de esa secuencia

alinear todas esas secuencias nucleotidicas con la secuencia nucleotidica
principal (se puede hacer con blast de manera local)


la que tenga un menor porcentaje de identidad sera la secuencia nucleotidica
nas modificada de la secuencia original pero que aun asi secuencie para la misma
proteina

# NO FUNCIONA SIEMPRE, Y FUNCIONA MAS LENTO QUE LAS OTRAS DOS

"""
from pysam import FastaFile
from itertools import product
from threading import Semaphore, Thread, Lock
from queue import Queue
import os

code = {
    "A": ["GCT", "GCC", "GCA", "GCG"],
    "R": ["CGT", "CGC", "CGA", "CGG", "AGA", "AGG"],
    "N": ["AAT", "AAC"],
    "D": ["GAT", "GAC"],
    "C": ["TGT", "TGC"],
    "Q": ["CAA", "CAG"],
    "E": ["GAA", "GAG"],
    "G": ["GGT", "GGC", "GGA", "GGG"],
    "H": ["CAT", "CAC"],
    "I": ["ATT", "ATC", "ATA"],
    "L": ["TTA", "TTG", "CTT", "CTC", "CTA", "CTG"],
    "K": ["AAA", "AAG"],
    "M": ["ATG"],
    "F": ["TTT", "TTC"],
    "P": ["CCT", "CCC", "CCA", "CCG"],
    "S": ["TCT", "TCC", "TCA", "TCG", "AGT", "AGC"],
    "T": ["ACT", "ACC", "ACA", "ACG"],
    "W": ["TGG"],
    "Y": ["TAT", "TAC"],
    "V": ["GTT", "GTC", "GTA", "GTG"],
    "X": ["TAA", "TAG", "TGA"]  # Stop codons
}
codon_table = {
    'TTT': 'F', 'TTC': 'F', 'TTA': 'L', 'TTG': 'L',
    'TCT': 'S', 'TCC': 'S', 'TCA': 'S', 'TCG': 'S',
    'TAT': 'Y', 'TAC': 'Y', 'TAA': 'X', 'TAG': 'X',
    'TGT': 'C', 'TGC': 'C', 'TGA': 'X', 'TGG': 'W',
    'CTT': 'L', 'CTC': 'L', 'CTA': 'L', 'CTG': 'L',
    'CCT': 'P', 'CCC': 'P', 'CCA': 'P', 'CCG': 'P',
    'CAT': 'H', 'CAC': 'H', 'CAA': 'Q', 'CAG': 'Q',
    'CGT': 'R', 'CGC': 'R', 'CGA': 'R', 'CGG': 'R',
    'ATT': 'I', 'ATC': 'I', 'ATA': 'I', 'ATG': 'M',
    'ACT': 'T', 'ACC': 'T', 'ACA': 'T', 'ACG': 'T',
    'AAT': 'N', 'AAC': 'N', 'AAA': 'K', 'AAG': 'K',
    'AGT': 'S', 'AGC': 'S', 'AGA': 'R', 'AGG': 'R',
    'GTT': 'V', 'GTC': 'V', 'GTA': 'V', 'GTG': 'V',
    'GCT': 'A', 'GCC': 'A', 'GCA': 'A', 'GCG': 'A',
    'GAT': 'D', 'GAC': 'D', 'GAA': 'E', 'GAG': 'E',
    'GGT': 'G', 'GGC': 'G', 'GGA': 'G', 'GGG': 'G'
}

bf_size = 100
filled_slots = Semaphore(0)
empty_slots = Semaphore(bf_size)
mtx = Lock()
buffer = Queue(maxsize = bf_size)
min = 100
#npp = 105  #importante que np y nc sean divisibles por 3
#ncc = 105

# Traduce de nucleotido a aminoacido
def translate(seq):
    protein = ""
    codon = ""
    for i in range(len(seq)):
        codon += seq[i]
        if len(codon) == 3:
            protein += codon_table[codon]
            codon = ""
    return protein

# Obtiene una secuencia de un fasta
def get_seq(filename, seqname):
    tmp = FastaFile(filename)
    os.remove("sequence.fasta.fai")
    return tmp.fetch(seqname)

# Cantidad de combinaciones
def comb_amount(amino_seq):
    total = 1
    for aa in amino_seq:
        total *= len(code[aa])
    return total

nuc_seq = get_seq("sequence.fasta", "XM_001608985.2")
amino_seq = translate(nuc_seq)

total = len(amino_seq)
posibilities = []
for i in range(total):
    posibilities.append(code[amino_seq[i]])
combinations = comb_amount(amino_seq)
npp = combinations//100
ncc = combinations//100
nc = 100
np = 100
"""nc = combinations // ncc
np = combinations // npp
if nc == 0:
    nc = 1
if np == 0:
    np = 1
if ncc > combinations:
    ncc = combinations
if npp > combinations:
    npp = combinations"""

# Calcular el porcentaje de identidad entre dos secuencias
def get_identity(seq1, seq2):
    if len(seq1) != len(seq2):
        return
    matches = 0
    for i in range(len(seq1)):
        if seq1[i] == seq2[i]:
            matches += 1
    id_percent = (matches/len(seq1)) * 100
    return id_percent


# Saca secuencia del buffer y compara
def consume(id):
    for i in range(ncc):
        filled_slots.acquire()
        mtx.acquire()
        global min
        global buffer
        seq = buffer.get()
        identity = get_identity(nuc_seq, seq)
        if identity < min:
            min = identity
        mtx.release()
        empty_slots.release()

# saca las combinaciones y las va agregando al buffer
def produce(id):
    # Generar todas las combinaciones posibles
    fseq = ""
    for i in range(id*npp, (id*npp + npp)):
        empty_slots.acquire()
        mtx.acquire()
        seq = [s for s in product(*posibilities)][i]
        for codon in seq:
            fseq += codon
        global buffer
        buffer.put(fseq)
        fseq = ""
        mtx.release()
        filled_slots.release()

def main():

    print(f"secuencia nucleotidica inicial: {nuc_seq}")
    print(f"secuencia aminoacidica: {amino_seq}")
    print(f"Numero de combinaciones: {combinations}")

    # crear los productores
    producers = []
    for i in range(np):
        producers.append(Thread(target=produce, args = (i,)))
        producers[i].start()
    consumers = []
    for j in range(nc):
        consumers.append(Thread(target=consume, args = (j,)))
        consumers[j].start()

    # que terminen
    for k in range(np):
        producers[k].join()
    for l in range(nc):
        consumers[l].join()

    print(f"Maximo porcentaje de cambio: {100-min}")

if __name__ == "__main__":
    main()
