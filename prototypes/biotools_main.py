"""
Tomar una secuencia nucleotidica
traducirla a secuencia aminoacidica

A partir de la secuencia aminoacidica retrotraducir todas las secuencias
nucleotidicas que se pueden sacar a partir de esa secuencia

alinear todas esas secuencias nucleotidicas con la secuencia nucleotidica
principal (se puede hacer con blast de manera local)


la que tenga un menor porcentaje de identidad sera la secuencia nucleotidica
nas modificada de la secuencia original pero que aun asi secuencie para la misma
proteina

FUNCIONA Y VA UN POQUITO MAS RAPIDO PERO NO TANTO

"""


from pysam import FastaFile
from itertools import product
import os
from Bio.Seq import Seq
from Bio import Align
from Bio.Align import substitution_matrices

code = {
    "A": ["GCT", "GCC", "GCA", "GCG"],
    "R": ["CGT", "CGC", "CGA", "CGG", "AGA", "AGG"],
    "N": ["AAT", "AAC"],
    "D": ["GAT", "GAC"],
    "C": ["TGT", "TGC"],
    "Q": ["CAA", "CAG"],
    "E": ["GAA", "GAG"],
    "G": ["GGT", "GGC", "GGA", "GGG"],
    "H": ["CAT", "CAC"],
    "I": ["ATT", "ATC", "ATA"],
    "L": ["TTA", "TTG", "CTT", "CTC", "CTA", "CTG"],
    "K": ["AAA", "AAG"],
    "M": ["ATG"],
    "F": ["TTT", "TTC"],
    "P": ["CCT", "CCC", "CCA", "CCG"],
    "S": ["TCT", "TCC", "TCA", "TCG", "AGT", "AGC"],
    "T": ["ACT", "ACC", "ACA", "ACG"],
    "W": ["TGG"],
    "Y": ["TAT", "TAC"],
    "V": ["GTT", "GTC", "GTA", "GTG"],
    "*": ["TAA", "TAG", "TGA"]  # Stop codons
}

def get_identity(seq1, seq2, aligner):
    alignment = aligner.align(seq1, seq2)[0]
    return (alignment.score/len(seq1))*100

# Combine sequences and calculate minimum identity
def combine(sequence, original):
    aligner = Align.PairwiseAligner()
    aligner.mode = "local"
    aligner.match_score = 1
    aligner.mismatch_score = 0
    aligner.gap_score = -100

    total = len(sequence)
    posibilities = [code[sequence[i]] for i in range(total)]

    min_identity = 100
    for seq in product(*posibilities):
        combined_raw = "".join(seq)
        combined_seq = Seq(combined_raw)
        identity = get_identity(original, combined_seq, aligner)
        if identity < min_identity:
            min_identity = identity
            #print(min_identity)
    return min_identity

# Get a sequence from a FASTA file
def get_seq(filename, seqname):
    tmp = FastaFile(filename)
    os.remove("sequence.fasta.fai")
    return tmp.fetch(seqname)

# Calculate the number of combinations
def comb_amount(amino_seq):
    total = 1
    for aa in amino_seq:
        total *= len(code[aa])
    return total

def main():
    nuc_raw = get_seq("sequence.fasta", "XM_001608985.2")
    nuc_seq = Seq(nuc_raw)
    amino_seq = nuc_seq.translate(table=1)
    print(f"Initial nucleotide sequence: {nuc_seq}")
    print(f"Amino acid sequence: {amino_seq}")
    print(f"Number of combinations: {comb_amount(amino_seq)}")
    print(f"Maximum percentage change: {100 - combine(amino_seq, nuc_seq)}")

if __name__ == "__main__":
    main()
