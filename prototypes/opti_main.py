"""
Tomar una secuencia nucleotidica
traducirla a secuencia aminoacidica

A partir de la secuencia aminoacidica retrotraducir todas las secuencias
nucleotidicas que se pueden sacar a partir de esa secuencia

alinear todas esas secuencias nucleotidicas con la secuencia nucleotidica
principal (se puede hacer con blast de manera local)


la que tenga un menor porcentaje de identidad sera la secuencia nucleotidica
nas modificada de la secuencia original pero que aun asi secuencie para la misma
proteina

FUNCIONA Y VA UN POQUITO MAS RAPIDO PERO NO TANTO

"""


from pysam import FastaFile
from itertools import product
import os
from Bio.Seq import Seq
from Bio import AlignIO, pairwise2
from Bio.Align import MultipleSeqAlignment
from Bio.Align.Applications import ClustalOmegaCommandline

code = {
    "A": ["GCT", "GCC", "GCA", "GCG"],
    "R": ["CGT", "CGC", "CGA", "CGG", "AGA", "AGG"],
    "N": ["AAT", "AAC"],
    "D": ["GAT", "GAC"],
    "C": ["TGT", "TGC"],
    "Q": ["CAA", "CAG"],
    "E": ["GAA", "GAG"],
    "G": ["GGT", "GGC", "GGA", "GGG"],
    "H": ["CAT", "CAC"],
    "I": ["ATT", "ATC", "ATA"],
    "L": ["TTA", "TTG", "CTT", "CTC", "CTA", "CTG"],
    "K": ["AAA", "AAG"],
    "M": ["ATG"],
    "F": ["TTT", "TTC"],
    "P": ["CCT", "CCC", "CCA", "CCG"],
    "S": ["TCT", "TCC", "TCA", "TCG", "AGT", "AGC"],
    "T": ["ACT", "ACC", "ACA", "ACG"],
    "W": ["TGG"],
    "Y": ["TAT", "TAC"],
    "V": ["GTT", "GTC", "GTA", "GTG"],
    "X": ["TAA", "TAG", "TGA"]  # Stop codons
}

codon_table = {
    'TTT': 'F', 'TTC': 'F', 'TTA': 'L', 'TTG': 'L',
    'TCT': 'S', 'TCC': 'S', 'TCA': 'S', 'TCG': 'S',
    'TAT': 'Y', 'TAC': 'Y', 'TAA': 'X', 'TAG': 'X',
    'TGT': 'C', 'TGC': 'C', 'TGA': 'X', 'TGG': 'W',
    'CTT': 'L', 'CTC': 'L', 'CTA': 'L', 'CTG': 'L',
    'CCT': 'P', 'CCC': 'P', 'CCA': 'P', 'CCG': 'P',
    'CAT': 'H', 'CAC': 'H', 'CAA': 'Q', 'CAG': 'Q',
    'CGT': 'R', 'CGC': 'R', 'CGA': 'R', 'CGG': 'R',
    'ATT': 'I', 'ATC': 'I', 'ATA': 'I', 'ATG': 'M',
    'ACT': 'T', 'ACC': 'T', 'ACA': 'T', 'ACG': 'T',
    'AAT': 'N', 'AAC': 'N', 'AAA': 'K', 'AAG': 'K',
    'AGT': 'S', 'AGC': 'S', 'AGA': 'R', 'AGG': 'R',
    'GTT': 'V', 'GTC': 'V', 'GTA': 'V', 'GTG': 'V',
    'GCT': 'A', 'GCC': 'A', 'GCA': 'A', 'GCG': 'A',
    'GAT': 'D', 'GAC': 'D', 'GAA': 'E', 'GAG': 'E',
    'GGT': 'G', 'GGC': 'G', 'GGA': 'G', 'GGG': 'G'
}


# Translate nucleotide to amino acid
def translate(seq):
    protein = []
    codon = ""
    for i in range(len(seq)):
        codon += seq[i]
        if len(codon) == 3:
            protein.append(codon_table[codon])
            codon = ""
    return "".join(protein)


def get_identity(seq1, seq2):
    if len(seq1) != len(seq2):
        return 0
    matches = sum(c1 == c2 for c1, c2 in zip(seq1, seq2))
    identity_percent = (matches / len(seq1)) * 100
    return identity_percent


# Combine sequences and calculate minimum identity
def combine(sequence, original):

    total = len(sequence)
    posibilities = [code[sequence[i]] for i in range(total)]

    min_identity = 100
    for seq in product(*posibilities):
        combined_seq = "".join(seq)
        identity = get_identity(original, combined_seq)
        if identity < min_identity:
            min_identity = identity
            print(min_identity)
    return min_identity


def all_seqs(sequence, original):
    total = len(sequence)
    return ["".join(codon) for codon in product(*([code[sequence[i]] for i in range(total)]))]


def to_fasta(seqs):
    text = ""
    for i in range(len(seqs)):
        text += f">seq{i+1}\n"
        text += f"{seqs[i]}\n"
    return text


# Get a sequence from a FASTA file
def get_seq(filename, seqname):
    tmp = FastaFile(filename)
    os.remove("sequence.fasta.fai")
    return tmp.fetch(seqname)


# Calculate the number of combinations
def comb_amount(amino_seq):
    total = 1
    for aa in amino_seq:
        total *= len(code[aa])
    return total


def main():
    nuc_seq = get_seq("sequence.fasta", "ref|NM_000558.5|:1-577")
    amino_seq = translate(nuc_seq)
    print(f"Initial nucleotide sequence: {nuc_seq}")
    print(f"Amino acid sequence: {amino_seq}")
    print(f"Number of combinations: {comb_amount(amino_seq)}")
    print("TODAS Y CADA UNA DE LAS SECUENCIAS VALIDAS")
    with open("seqs.fasta", "w") as file1:
        file1.write(to_fasta(all_seqs(amino_seq, nuc_seq)))
    
if __name__ == "__main__":
    main()
