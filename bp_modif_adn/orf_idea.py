"""

EXPLICACION GENERAL DEL PROGRAMA

Se lee desde un archivo fasta una secuencia, que se asume será una secuencia de RNA maduro.
Se buscan todos los marcos de lectura dentro de esta secuencia de RNA maduro.
Se utiliza el ORF mas grande para trabajar.
Se traduce este ORF a secuencia aminoacidica.
A partir de esta secuencia aminoacidica, se busca la secuencia nucleotidica que cumpla
con cierta proporcion de identidad de secuencia traducia con la secuencia aminoacidica original.
Se calculan identidades de secuencias nucleotidicas (los orf calculados) y aminoacidicas.
Posterior a esto se vuelven a calcular porcentajes de identidad, pero ahora considerando el mRNA completo
y no solamente los orfs.

* Se asume que se trabaja con un mRNA
* Cuando se reemplazan valores en las secuencias, ya sea nucleotidica o aminoacidica, se asume:
    ** En la secuencia nucleotidica no mutará de tal forma que se cree otro marco de lectura más grande,
       que gane prioridad sobre el marco trabajado
    ** No se insertarán codones de stop dentro de la secuencia aminoacidica, que interrumpan y corten la
       proteina nueva.

DESCARGAR DE UNA BASE DE DATOS DE MRNA Y HACER MUCHAS VECES ESTE PROGRAMA
METER LOS DATOS Y HACER UN HISTOGRAMA EN R PARA CADA UNO
HACER EL INFORME Y MANDARLO

"""


from Bio import SeqIO
from Bio.Seq import Seq
from itertools import product
import json


with open("create_first_json.json") as json_file:
    most_diff_relative = json.load(json_file)

with open("create_second_json.json") as json_file:
    most_sim_outsider = json.load(json_file)

with open("code.json") as json_file:
    code = json.load(json_file)

with open("codon_table.json") as json_file:
    codon_table = json.load(json_file)
    

# Calcula el porcentaje de identidad entre dos secuencias
# alinea sin permitir gaps ni nada de eso, solamente se usan secuencias del mismo largo
def get_identity(seq1, seq2):
    if len(seq1) != len(seq2):
        return 0
    matches = sum(c1 == c2 for c1, c2 in zip(seq1, seq2))
    identity_percent = (matches / len(seq1)) * 100
    return identity_percent

# Se encuentran ORFS dentro de una secuencia nucleotidica
# Se parte con la M y se termina con un codon de stop *
def find_orfs(dna_sequence):
    dna_sequence = Seq(dna_sequence)
    orfs = []
    stops = ["TAA", "TAG", "TGA"]
    for frame in range(3): # 3 marcos de lectura por hebra
        for strand in range(2): # 5'-3' o 3'-5', haciendo 6 en total
            if strand == 0:
                seq = dna_sequence[frame:]
            else:
                seq = dna_sequence.reverse_complement()[frame:]
            codons = split_codons(seq)
            # Ahora que se tiene la secuencia con la que se va a trabajar sacar los orfs
            # meterse en los codones y ver el cuadro adentro
            orf = ""
            keep_adding = False
            for codon in codons:
                if keep_adding:
                    orf += codon
                
                if codon == "ATG":
                    orf += codon
                    keep_adding = True
                    
                if codon in stops:
                    orfs.append(orf)
                    orf = ""
                    keep_adding = False
    return orfs

# Se toma una secuencia nucleotidica y se parte en codones
# se parte desde el inicio de la secuencia, a la funcion ya se le entrga una secuencia previamente
# procesada para estar en el marco de lectura correcto
# es posible que dependiendo del largo de la secuencia, al final queden uno o dos nucleotidos sueltos
# eso es normal
def split_codons(dna_sequence):
    codons = []
    codon = ""
    for nuc in dna_sequence:
        codon += nuc
        if len(codon) == 3:
            codons.append(codon)
            codon = ""
    return codons

# Devuelve la secuencia mas larga de una lista de secuencias
def biggest(any_seqs):
    biggest = ""
    for any_seq in any_seqs:
        if len(any_seq) > len(biggest):
            biggest = any_seq
    return biggest
        

# Lee un fasta
def read_fasta_file(file_path):
    sequences = []
    for record in SeqIO.parse(file_path, "fasta"):
        sequence = record.seq
        sequences.append(sequence)
    return sequences


# Devuelve la secuencia nucleotidica mas diferente de la secuencia original, considerando un cierto
# porcentaje de identidad de secuencia aminoacidica objetvivo
# Si la proporcion es 1, entonces 100% de identidad aminoacidica
# si es 0.4, entonces 40% de secuencia aminoacidica y asi
# Si corresponde, el resto de la secuencia se rellenara con "NNN", que significa "!" en aminoacido
# representa cualquier elemento que sea distinto al elemento al cual se estara comparando
# Se usa esta "licencia" para simplificar calculos y todo eso
def get_most_diff_seq(original, proportion):
    seq2 = ""
    for i in range(0, int(len(original)*proportion), 3):
        codon = original[i:i+3]
        seq2 += most_diff_relative[codon]
    for i in range((len(original) - len(seq2))//3):
        seq2 += "NNN"
    return seq2


# Se quiere un 30% de secuencia aminoacidica con la secuencia original, con el mínimo de mutaciones
# Para el 30% de la secuencia se mantienen iguales los codones, para el 70% restante se rellena con
# un codon que secuencie para un aminoacido distinto, intentando mantener la secuencia aminoacidica lo más grande
# posible
def get_most_sim_seq(original, proportion):
    seq2 = ""
    limit = int(len(original) * proportion)
    for i in range(0, int(len(original)), 3):
        codon = original[i:i+3]
        if i > limit:
            codon = most_sim_outsider[codon]
        seq2 += codon
    return seq2


def translate(nuc_seq):
    codons = split_codons(nuc_seq)
    aminoacid = ""
    for codon in codons:
        aminoacid += codon_table[codon]
    return aminoacid


def report_max_div(dna_sequence, proportion):
    orfs = find_orfs(dna_sequence)
    nuc_orf = biggest(orfs)
    amino_orf = translate(nuc_orf)
    most_diff_nuc = get_most_diff_seq(nuc_orf, proportion)
    diff_amino = translate(most_diff_nuc)
    id_between_nucs = get_identity(nuc_orf, most_diff_nuc)
    final_id = (id_between_nucs * len(nuc_orf))/len(dna_sequence)
    amino_id = get_identity(amino_orf, diff_amino)

    print(f"==== CON LA MAYOR DIVERGENCIA POSIBLE =============================================================")
    print(f"Para tener un {proportion*100}% de identidad de secuencia aminoacidica:")
    print(f"Identidad entre marcos de lectura: {id_between_nucs}% - {100-id_between_nucs}% de diferencia")
    print(f"Comparando a nivel de mRNA y no solo marco de lectura: {final_id}% - {100-final_id} % de diferencia")
    print(f"Porcentaje de identidad entre aminoacidos: {amino_id}%\n\n")

    #print(f"ORF traducido original: {amino_orf}")

def report_min_div(dna_sequence, proportion):
    orfs = find_orfs(dna_sequence)
    nuc_orf = biggest(orfs)
    amino_orf = translate(nuc_orf)
    most_sim_nuc = get_most_sim_seq(nuc_orf, proportion)
    sim_amino = translate(most_sim_nuc)
    id_between_nucs = get_identity(nuc_orf, most_sim_nuc)

    final_id = (id_between_nucs * len(nuc_orf))/len(dna_sequence)
    amino_id = get_identity(amino_orf, sim_amino)

    print(f"==== CON LA MENOR DIVERGENCIA POSIBLE =============================================================")
    print(f"Para tener un {proportion*100}% de identidad de secuencia aminoacidica:")
    print(f"Identidad entre marcos de lectura: {id_between_nucs}% - {100-id_between_nucs}% de diferencia")
    print(f"Comparando a nivel de mRNA y no solo marco de lectura: {final_id}% - {100-final_id} % de diferencia")
    print(f"Porcentaje de identidad entre aminoacidos: {amino_id}%\n\n")

    #print(f"AMINOACIDO NUEVO: {sim_amino}")
    
    
def main(): 
    
    dna_sequence = read_fasta_file("sequence.fasta")[0]

    report_max_div(dna_sequence, 1)
    report_min_div(dna_sequence, 0.3)
    
    
if __name__ == "__main__":
    main()
