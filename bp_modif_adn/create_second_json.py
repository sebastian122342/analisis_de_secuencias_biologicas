# La idea es tomar el codigo genetico y formar otro diccionario donde en cada key haya un codon
# y como value este el otro codon mas similar del codon original pero que aun asi traduzcan para
# aminoacidos diferentes

# ARREGLAR ESTA PARTE - VERIFICAR
# MUCHAS VECES SE PONE COMO CODON MAS SIMILAR EL CODON DE STOP
#
#

import json
from itertools import product

code = {
    "A": ["GCT", "GCC", "GCA", "GCG"],
    "R": ["CGT", "CGC", "CGA", "CGG", "AGA", "AGG"],
    "N": ["AAT", "AAC"],
    "D": ["GAT", "GAC"],
    "C": ["TGT", "TGC"],
    "Q": ["CAA", "CAG"],
    "E": ["GAA", "GAG"],
    "G": ["GGT", "GGC", "GGA", "GGG"],
    "H": ["CAT", "CAC"],
    "I": ["ATT", "ATC", "ATA"],
    "L": ["TTA", "TTG", "CTT", "CTC", "CTA", "CTG"],
    "K": ["AAA", "AAG"],
    "M": ["ATG"],
    "F": ["TTT", "TTC"],
    "P": ["CCT", "CCC", "CCA", "CCG"],
    "S": ["TCT", "TCC", "TCA", "TCG", "AGT", "AGC"],
    "T": ["ACT", "ACC", "ACA", "ACG"],
    "W": ["TGG"],
    "Y": ["TAT", "TAC"],
    "V": ["GTT", "GTC", "GTA", "GTG"],
    "*": ["TAA", "TAG", "TGA"]  # Stop codons
}


utility_code = {}


# Crear un diccionario donde en cada key haya un codon y como value esten todos los codones que traduzcan para
# algo diferente

most_sim_outsider = {}


# Recibir un codon y devolver una lista con todos los codones que no codifiquen para el mismo aminoacido
# se evita que se coloquen codones de stop en la lista de outsiders
# La restriccion no aplica para los codones de stop, estos no pueden ser eliminados
# Tambien se tendria que aplicar eso para los codones de inicio, pero como al hacer los calculos se mantienen el primer 30% de
# los codones, no es necesario

def get_outsiders(codon):
    temp_list = []
    if codon in code["*"]:
        temp_list = code["*"]
        return temp_list
    for aa in code.keys():
        if not codon in code[aa]:
            for outsider in code[aa]:
                if not outsider in code["*"]:
                    temp_list.append(outsider)
    return temp_list


# crear el diccionario donde en cada key haya un codon y en los values todos los codones que no traducen al mismo aminoacido
def create_utility_code():
    for aa in code.keys():
        for codon in code[aa]:
            utility_code[codon] = get_outsiders(codon)
            

# Compara el codon con cada uno de los demas y devuele el menos parecido
# solo codones que codifiquen para otro aminoacido
def most_sim(codon, outsider):
    first = [outsider]
    second = utility_code[codon]

    max_codon = ""
    max_id = 0
    for seq in product(first, second):
        identity = get_identity(seq[0], seq[1])
        if identity >= max_id:
            max_codon = seq[1]
            max_id = identity
    return max_codon


# Saca las diferencias entre codones
def get_sims():
    for codon in utility_code.keys():
        most_sim_outsider[codon] = most_sim(codon, utility_code[codon])

            
def get_identity(seq1, seq2):
    if len(seq1) != len(seq2):
        return 0
    matches = sum(c1 == c2 for c1, c2 in zip(seq1, seq2))
    identity_percent = (matches / len(seq1)) * 100
    return identity_percent


def main():
    create_utility_code()
    get_sims()
    with open("create_second_json.json", "w") as outfile:
        json.dump(most_sim_outsider, outfile)

if __name__ == "__main__":
    main()
