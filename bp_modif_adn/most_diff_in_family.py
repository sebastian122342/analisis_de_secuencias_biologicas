# Se crea el json donde en cada key hay un codon y en cada value el codon
# mas distinto al codon original que aun asi secuencie para el mismo aminoacido

import json
from itertools import product

code = {
    "A": ["GCT", "GCC", "GCA", "GCG"],
    "R": ["CGT", "CGC", "CGA", "CGG", "AGA", "AGG"],
    "N": ["AAT", "AAC"],
    "D": ["GAT", "GAC"],
    "C": ["TGT", "TGC"],
    "Q": ["CAA", "CAG"],
    "E": ["GAA", "GAG"],
    "G": ["GGT", "GGC", "GGA", "GGG"],
    "H": ["CAT", "CAC"],
    "I": ["ATT", "ATC", "ATA"],
    "L": ["TTA", "TTG", "CTT", "CTC", "CTA", "CTG"],
    "K": ["AAA", "AAG"],
    "M": ["ATG"],
    "F": ["TTT", "TTC"],
    "P": ["CCT", "CCC", "CCA", "CCG"],
    "S": ["TCT", "TCC", "TCA", "TCG", "AGT", "AGC"],
    "T": ["ACT", "ACC", "ACA", "ACG"],
    "W": ["TGG"],
    "Y": ["TAT", "TAC"],
    "V": ["GTT", "GTC", "GTA", "GTG"],
    "*": ["TAA", "TAG", "TGA"]  # Stop codons
}

most_diff_codon = {}

# Compara el codon con cada uno de los demas y devuele el menos parecido
# solo codones que codifiquen para el mismo aminoacido
def most_diff(aminoacid, codon):
    first = [codon]
    second = code[aminoacid]

    min_codon = ""
    min_id = 100
    for seq in product(first, second):
        identity = get_identity(seq[0], seq[1])
        if identity <= min_id:
            min_codon = seq[1]
            min_id = identity
    return min_codon


# Saca las diferencias entre codones
def get_diffs():
    for aa in code.keys():
        for codon in code[aa]:
            most_diff_codon[codon] = most_diff(aa, codon)

def get_identity(seq1, seq2):
    if len(seq1) != len(seq2):
        return 0
    matches = sum(c1 == c2 for c1, c2 in zip(seq1, seq2))
    identity_percent = (matches / len(seq1)) * 100
    return identity_percent



def main():
    get_diffs()
    with open("most_diff_relative.json", "w") as outfile:
        json.dump(most_diff_codon, outfile)


if __name__ == "__main__":
    main()
