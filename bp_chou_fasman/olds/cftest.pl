# test de chou-fasman

use strict;
use warnings;

# Toma la tabla de puntaje de aminoacidos y la hace hash de referencias a hashes
sub process_table{

    # La idea es poner la info de la tabla a modo de hash de hashes
    # cada key es el codigo de una letra de cada aminoacido
    # cada value es un hash que tiene todos los valores para ese aminoacido
    # como key se tiene la letra del aminoacido, como value se tiene la referencia a un hash
    # que tiene los keys y values para ese aminoacido en especifico
    
    my $raw_table = shift;
    my @all_lines = split /\n/, $raw_table;
    #my %temp_hash;
    my %full_hash;
    my @current_line;
    
    my @header = split /\t/, $all_lines[0];
    
    for (my $i=1; $i < scalar @all_lines; $i++){
	my %temp_hash;
	@current_line = split /\t/, $all_lines[$i];
	for (my $j=3; $j < scalar @header; $j++){
	    my $num = sprintf("%.3f", $current_line[$j]);
	    $temp_hash{$header[$j]} = $num;  
	}
	# print_hash(\%temp_hash);
	$full_hash{$current_line[2]} = \%temp_hash;
    }
    
    return \%full_hash;
}

# Saca el texto de una tabla, funciona
sub file_text{
    my $file = shift;
    my $text = "";
    
    open(FH, "<", $file) or die $!;
    while (<FH>){
	$text = $text . $_;
    }
    close (FH);
    return $text;
}

sub find_regions{
    my ($cf_ref, $consec, $win_size, $seq_str, $key, $alt_key, $symbol) = @_;
    # ref de chou-fasman, cuantos consecutivos deben ser (3 de 5 o 4 de 6), tamaño de ventana (5 o 6)
    # la secuencia en string, la key "P(a)" o "P(b)", $symbol es "H" o "B"

    # arreglo donde cada elemento es un arreglo de dos elementos donde en el primero esta el inicio
    # y el segundo el final de la ventana válida
    my @prediction = [];

    # considerar partir desde el 4 y terminar en length($seq_str)-4 por el tema de los bordes
    for(my $i=0; $i<length($seq_str)-$win_size; $i++){
	my $window = substr($seq_str, $i, $win_size);
	#print($window, " ventana inicial\n");
	# verificar que en la ventana hayan $consec elementos con puntaje a o b mayor que 100
	if (check_window($window, $consec, $key, $cf_ref)){
	    #print("ventana valida!!!\n");
	    # extender hacia ambos lados hasta pillar cuatro aas con puntaje promedio menor que 100
	    # la window ahora mismo va desde $i hasta $i+$win_size
	    my $right_side = substr($seq_str, $i+$win_size);
	    my $left_side = reverse substr($seq_str, 0, $i);
	    my $add_right = check_extension($right_side, $cf_ref, $key);
	    my $add_left = reverse check_extension($left_side, $cf_ref, $key);
	    my $segment = $add_left . $window . $right_side;
	    #print("segmento final: $segment\n");
	    # revisar el segmento, longitud mayor que 5 y que pa sea mayor que pb promedio ambas

	    # window va desde $i a $i+$win_size
	    # ahora que se agregaron los segmentos es posible que se haya extendido la ventana
	    # ahora terminaria en $i+$win_size + length($add_right)
	    # y por el lado izquierdo partiria desde antes, asi que empezaria en
	    # $i - length($add_left)

	    # no se esta chequeando lo de segment valido
	    my @struc = ($i-length($add_left), $i+$win_size+length($add_right));
	    #print("en la otra parte: $struc[0], $struc[1] \n");
	    push (@prediction, \@struc);
	    #if (segment_is_valid($segment, $cf_ref, $key, $alt_key)){
		#print("HOLAAAAAAAAAAAAA\n");
		# añadir indice de inicio y fin al arreglo de predicciones
		# habria que ver cual es el inicio y fin de segment dentro de la secuencia
		# quizas modificar las subrutinas de check_segment para que ademas devuelvan
		# un indice que sea util
		#my @struc = ($i-length($add_left), $i+$win_size+length($add_right));
		#print("\nComparacion de cosas\n");
		#print("p: ", substr($seq_str, $struc[0], $struc[1]), "\n");
		#print("s: ", $segment, "\n");
		#print("holi: @struc\n");
		#push (@prediction, \@struc); 
	    #}
	}
    }
    #print(scalar @prediction, "HOLA2\n");
    return \@prediction
}

sub check_window{
    #my %hash = %$cf_ref;
    #my $m_data_ref = %hash{"M"};
    #my %m_data = %$m_data_ref;
    
    my ($window, $consec, $key, $cf_ref) = @_;
    my %cf_hash = %$cf_ref;
    my $streak = 0;
    
    for(my $i=0; $i < length($window); $i++){
	my $aa = substr($window, $i, 1);
	my $data_ref = $cf_hash{$aa};
	my %data = %$data_ref;

	if ($streak == 4){
	    return 1;
	}
	
	if ($data{$key} > 100){
	    $streak++;
	}else{
	    $streak = 0;
	}
    }
    return 0; # just for good measure (??)
}

sub check_extension{
    # ir agregando a una string hasta que se pillen 4 consecutivos con puntaje a o b menor que 100 promedio
    my ($ext_str, $cf_ref, $key) = @_;
    my $mini_seq = "";
    my %cf_hash = %$cf_ref;
    my $sum = 0;
    my $avg;
    
    for (my $i=0; $i < length($ext_str)-4; $i++){
	my $aa = substr($ext_str, $i, 1);
	my $data_ref = $cf_hash{$aa};
	my %data = %$data_ref;
	
	my $win = substr($ext_str, $i, 4);
	for(my $j=0; $j < length($win); $j++){
	    $sum += $data{$key}
	}
	$avg = $sum/4;
	
	if ($avg < 100){
	    return $mini_seq;
	}else{
	    $sum = 0;
	    $mini_seq = $mini_seq . $aa;
	}
    }
    return $mini_seq; # just for good measure (??)
}

# tomar el arreglo de arreglos con las coordenadas donde empiezan y terminan las cosas y construir una string del estilo
# ---HHH-----HHHHHH-HHH-H-HHHH-HHHH-HHH-H-H y asi
sub build{

    my ($seq_length, $symbol, $pred_ref) = @_;
    my @preds = @$pred_ref;
    #print(scalar @preds, "BUENAS TARDES\n");
    my $temp_str = "";

    for my $i (0..$seq_length-1){
	$temp_str = $temp_str . "-";
    }

    my @seq = split //, $temp_str;
    
    foreach my $el_ref (@preds){
	my @el = @$el_ref;
	if (scalar @el == 0){
	    next;
	}
	my $start = $el[0];
	my $end = $el[1];
	# ERROR ACA A VECES START Y END NO INICIALIZADOS DICE
	#print "inicio: $start - fin: $end\n";
	for(my $j=$start; $j<$end; $j++){
	    @seq[$j] = $symbol;
	}
    }
    print("analizadita: @seq\n");
    return \@seq;
}

# arreglar estas cosas
sub find_turns{
    my ($seq_str, $cf_ref) = @_;

    my @prediction;
    my %cf_hash = %$cf_ref;
    
    for(my $j=0; $j < length($seq_str)-4; $j++){
	my $win = substr($seq_str, $j, 4);
	my @els = split//, $win;

	my $prod_turn = 1; # f(i)*f(i+1)*f(i+2)*f(i+3)
	my $sum_alphas = 0;
	my $sum_betas = 0;
	my $sum_t = 0; #P(t)


	my $n = 0;
	foreach my $el (@els){
	    my $data_ref = $cf_hash{$el};
	    my %data = %$data_ref;
	    my $key;
	    if ($n == 0){
		$key = "f(i)";
	    }else{
		$key = "f(i+$n)";
	    }
	    $prod_turn = $prod_turn * $data{$key};
	    $sum_alphas += $data{"P(a)"};
	    $sum_betas += $data{"P(b)"};
	    $sum_t += $data{"P(t)"};
	    $n++;
	}
	
	#my $curr = substr($seq_str, $j, 1);
	#my $data_curr_ref = $cf_hash{$curr};
	#my %data_curr = %$data_curr_ref;
	
	my $avg_t = $sum_t/4;
	my $avg_alpha = $sum_alphas/4;
	my $avg_beta = $sum_betas/4;
	# revisar segun el valor de avg ver si el residuo de $i es un t

	if ($avg_t > 100 && $prod_turn > 0.000075 && $avg_t > $avg_beta && $avg_t > $avg_alpha){
	    push(@prediction, "T");
	}else{
	    push(@prediction, "-");
	}
    }

    # Rellenar los ultimos 4 restantes
    for my $i (0..3){
	push(@prediction, "-");
    }

    #print(scalar @prediction, "\n");
    print("Turns: @prediction\n");
    return \@prediction;
}


# Saca las coordenadas de las regiones donde se solapan las estructuras secundarias
sub get_overlapping{

    my ($alphas_ref, $betas_ref, $seq_str) = @_;

    my @intersections;
    my $started = 0;

    my @alphas = @$alphas_ref;
    my @betas = @$betas_ref;
    my $start;
    my $end;
    #my @inter;

    for(my $i=0; $i < length($seq_str); $i++){
	if ($started){
	    if ($alphas[$i] eq "-" || $betas[$i] eq "-"){
		# se acabo la overlapping zone
		$end = $i-1;
		my @inter = ($start, $end);
		#print("interseccion: @inter\n");
		push(@intersections, \@inter);
		$started = 0;
	    }
	}else{
	    if ($alphas[$i] ne "-" && $betas[$i] ne "-"){
		$start = $i;
		$started = 1;
	    }
	}
    }
    # en caso de que coincidan hasta el final, chequear aqui
    if ($started){
	$end = length($seq_str)-1; #indices parten del 0
	my @inter = ($start, $end);
	#print("interseccion: @inter\n");
	push(@intersections, \@inter);
    }
    
    return \@intersections;
}


sub process_intersections{

    my ($seq_str, $cf_ref, $inters_ref) = @_;

    my @dominant;
    my $symbol;

    for(my $j=0; $j<length($seq_str); $j++){
	push(@dominant, "-");
    }
    
    my @inters = @$inters_ref;
    my %hash = %$cf_ref;

    my $alpha_sum = 0;
    my $beta_sum = 0;
    
    foreach my $inter_ref (@inters){
	my @inter = @$inter_ref;
	#print("trabajando con: @inter\n");
	my $n_els = $inter[1] - $inter[0] + 1;
	for my $i ($inter[0]..$inter[1]){

	    my $aa = substr($seq_str, $i, 1);
	    my $data_ref = $hash{$aa};
	    my %data = %$data_ref;

	    $alpha_sum += $data{"P(a)"};
	    $beta_sum += $data{"P(b)"};
	}
	my $alpha_avg = $alpha_sum/$n_els;
	my $beta_avg = $beta_sum/$n_els;
	    
	if ($alpha_avg > $beta_avg){
	    $symbol = "H";
	}else{
	    $symbol = "B";
	}
	    
	for my $k ($inter[0]..$inter[1]){
	    $dominant[$k] = $symbol;
	}
	$alpha_sum = 0;
	$beta_sum = 0;
    }

    print("interseccion procesada: @dominant\n");
    return \@dominant;
}

sub merge_abs{
    my ($alpha_ref, $beta_ref, $cf_ref, $seq_str) = @_;
    # aqui se va a a seguir las instrucciones del pdf mas que las de la pagina
    # en general todo el programa es una mezcla de las instrucciones de la pagina y del pdf
    # Si no se topa entonces se guarda la que haya nomas, si no hay nada se pone un -
    # para cuando se topen, ver la zona total donde se topan, como la interseccion
    # sacar los promedios de puntaje de cada una de las coasas, el que tenga mas alto gana
    # el que gana es insertado en la mergeada

    my @alphas = @$alpha_ref;
    my @betas = @$beta_ref;
    my $inters_ref = get_overlapping($alpha_ref, $beta_ref, $seq_str);
    my $dom_ref = process_intersections($seq_str, $cf_ref, $inters_ref);
    my @dominant = @$dom_ref;

    my @merged;
    
    for (my $i=0; $i < length($seq_str); $i++){
	if ($alphas[$i] eq $betas[$i]){
	    push(@merged, "-");
	}else{
	    if ($alphas[$i] eq "-"){
		push(@merged, $betas[$i]);
	    }elsif($betas[$i] eq "-"){
		push(@merged, $alphas[$i]);
	    }else{
		push(@merged, $dominant[$i]);
	    }
	}
    }

    print("merged ab: @merged\n");
    
    return \@merged;
    
    #print(scalar @merged, "\n");
    #print("*********\n");
    #print("alfas: ", scalar @alphas, "\n");
    #print("betas: ", scalar @betas, "\n");

    # a este punto ya se sabe que el largo de las alfas y las betas es el mismo
    
}

# insertar los turns en la mezcla de alphas y betas, tienen prioridad sobre estas siempre
sub insert_turns{
    my ($merged_ab_ref, $turns_ref) = @_;

    #print(scalar @$merged_ab_ref, "\n");
    #print(scalar @$turns_ref, "\n");

    my @merged_ab = @$merged_ab_ref;
    my @turns = @$turns_ref;

    my @result;
    
    for(my $i=0; $i < scalar @turns; $i++){

	if ($turns[$i] ne "-"){
	    push(@result, $turns[$i]);
	}else{
	    push(@result, $merged_ab[$i]);
	}
    }

    print("Resultado sin chequeo de largos: @result\n");

    return \@result;
}

# Separar la secuencia entre grupos
# cada vez que se cambie de cosa en la secuencia se construye un grupo
# cuando se habla de turn no importa que no sea de largo menor que 5
sub check_struc_length{
    
    my $seq_ref = shift;

    my @seq = @$seq_ref;

    my $current;
    my $started;
    
    my @group;
    my @final;
    
    $started = 0;
    foreach my $aa (@seq){
	if ($started){
	    if ($aa eq $current){
		push(@group, $aa);
	    }else{
		#print("grupoz2: @group\n");

		if (scalar @group < 5 && $current ne "T"){
		    foreach my $el (@group){
			push(@final, "-");
		    }
		}else{
		    foreach my $el (@group){
			push(@final, $el);
		    }
		}
		
		#push(@groups, \@group);
		@group = ();
		
		$current = $aa;
		push(@group, $aa);
	    }
	}else{
	    $current = $aa;
	    push(@group, $aa);
	    #print("grupo: @group\n");
	    $started = 1;
	}
    }

    if (scalar @group > 5){
	foreach my $el (@group){
	    push(@final, $el);
	}
    }else{
	foreach my $el (@group){
	    push(@final, "-");
	}
    }

    print("Con el largo revisado: @final\n");
    return \@final;
}


sub replace{
    my ($arr_ref, $replaced, $new_el) = @_;
    my @arr = @$arr_ref;
    my @final;
    
    foreach my $el (@arr){
	if ($el eq $replaced){
	    push(@final, $new_el)
	}else{
	    push(@final, $el);
	}
    }

    return \@final;
}

sub chou_fasman{
    my ($seq_str, $cf_ref) = @_;
    #my @seq = split(//, $seq_str);

    
    # ya se sabe el puntaje de cada uno de los aminoacidos via tabla
    # escanear la secuencia y ver todas las secciones donde 4 de 6 aminoacidos consecutivos tenga palpha > 100
    # ir extendiendo cada region a derecha e izquierda e ir agregando nucleotidos a la helice,
    # hasta que se pille una zona donde el promedio de 4 nucleotidos en palpha sea < 100, ese es el final
    # de la helice.
    #si el segmento que resulta de todo esto tiene un largo menor que 5 y el promedio alpha es mayor al
    # promedio beta, el segmento se asigna como helice

    # equivalente para vela, pero con 3 de 5 aminoacidos

    # si hay regiones sobrelapadas de alphas y betas, la que tenga un promedio mayor domina sobre la otra

    #my ($cf_ref, $consec, $win_size, $seq_str, $key, $alt_key, $symbol) = @_;
    # ref de chou-fasman, cuantos consecutivos deben ser (3 de 5 o 4 de 6), tamaño de ventana (5 o 6)
    # la secuencia en string, la key "P(a)" o "P(b)", $symbol es "H" o "B"

    my $alphas_ref = find_regions($cf_ref, 4, 6, $seq_str, "P(a)", "P(b)", "H");
    my $alphas_build_ref = build(length($seq_str), "H", $alphas_ref);

    print("----------------------------------------------------------------------------\n\n");
    my $betas_ref = find_regions($cf_ref, 3, 5, $seq_str, "P(b)", "P(a)", "B");
    my $betas_build_ref = build(length($seq_str), "B", $betas_ref);

    print("----------------------------------------------------------------------------\n\n");
    my $turns_ref = find_turns($seq_str, $cf_ref);

    #print("seq: ", length($seq_str), "\n");
    #print("turns: ", scalar @$turns_ref, "\n");
    my $merged_ab_ref = merge_abs($alphas_build_ref, $betas_build_ref, $cf_ref, $seq_str);

    my $final_unchecked_ref = insert_turns($merged_ab_ref, $turns_ref);
    my $final_nocoil_ref = check_struc_length($final_unchecked_ref);
    my $final_ref = replace($final_nocoil_ref, "-", "C");
    my @final = @$final_ref;
    print("\n\n\n\nFinal: @final\n");
    #print(scalar @final, "\n\n");
}


sub main{

    my $seq = "MNDIIFAKVTADGICAVNIVKLSGKNVNKLIFPLIKKKLKKQKMIYTNLYGIKEKYTEKILIVFFKSPNTFTGEDLIEFHLNGNYCLLNKLIKDLIFLGVRPAKPGEFLERRYSSGKITLFECEIINDKILYSYTNMFKLTMEDQKNFYLSIIQNLKFKFNIIIICLECFCIFNKNSLKKDFIFIKNFFKKIKKLINILQIKIEKIDYLKNKFEIMIIGRRNVGKSTLFNKLCLQYGSIVTNIPGTTKNNINKQIFFSSKKININDTAGIKIKSKNLIEKIGILKNINKIFKSSLILYIVDEFNLKKSLFNAPLNIYEKIIQNKIIILINKCDIIGIKEGIFKLNNIIIIFICVKNNNLINKIKCFISKIVENEKILINSKFCYSNIQLLLDNCKIFYKNFFCSYDIISENIIRFQKKIYKLTGEYINETSINFLFRNFCVGK";

    my $aa_table = file_text("chou-fasman_table.txt");
    my $cf_ref = process_table($aa_table);

    chou_fasman($seq, $cf_ref);
}

main();

=begin
sub segment_is_valid{
    # revisar largo mayor que 5 y promedio key > promedio alt_key en el segmento
    my ($segment_str, $cf_ref, $key, $alt_key) = @_;
    if (length($segment_str) <= 5){
	return 0;
    }
    my %cf_hash = %$cf_ref;
    my $sum = 0;
    my $alt_sum = 0;
    
    for (my $i=0; $i < length($segment_str); $i++){
	my $aa = substr($segment_str, $i, 1);
	my $data_ref = $cf_hash{$aa};
	my %data = %$data_ref;

	$sum += $data{$key};
	$alt_sum += $data{$alt_key};
    }
    my $avg = $sum/length($segment_str);
    my $alt_avg = $alt_sum/length($segment_str);

    #print($avg, " ", $alt_avg, " HEYYY \n\n");
    
    if ($avg > $alt_avg){
	return 1;
    }
    return 0;
}
=cut

