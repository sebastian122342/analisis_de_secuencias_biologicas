from Bio.PDB import PDBParser
from Bio.PDB.DSSP import DSSP

# Replace 'your_pdb_file.pdb' with the path to your PDB file
pdb_file = "5u09.pdb"

# Parse the PDB file
parser = PDBParser(QUIET=True)
structure = parser.get_structure("protein", pdb_file)

# Calculate the secondary structure using DSSP
model = structure[0]
dssp = DSSP(model, pdb_file)

# Extract secondary structure information
sequence = ""
secondary_structure = ""

for res in dssp:
    sequence += res[1]
    secondary_structure += res[2]

print("Protein sequence:", sequence)
print("Secondary structure:", secondary_structure)

