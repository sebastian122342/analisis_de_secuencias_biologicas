#!/usr/bin/perl

# See tiene que aplicar el metodo chou-fasman de prediccion de la estructura
# secuendaria de una proteina, a partir de la secuencia aminoacidica.
# se debe armar un fasta con todas las CDS traducidas a proteina, ese archivo
# ya se encuentra un archivo con todo eso en genbank, asi que trabajar con eso por mientras
# aplicar el metodo a todas las secuencias y armar el format usando el mismo format de perl

# VERIFICAR QUE SE ESTE HACIENDO BIEN EL PROCESADO DEL GBFF
# APLICAR EL METODO DE CHOU-FASMAN
# CONSIDERAR LA OPCION DE PENSAR CADA AMINOACIDO COMO UN OBJETO QUE TENGA CADA PUNTAJE ASOCIADO
# CONSIDERAR PASARLO TODO A ALGO ORIENTADO A OBJETOS


use strict;
use warnings;
use Bio::SeqIO;

# Recibe la linea "desc" del archivo de traducciones
# La procesa y crea un hash, devuelve la referencia
sub process_desc {
    my $line = shift;
    my @arr = split //, $line;
    my %final_hash;

    my $temp_str = "";
    my $add = 0;

    foreach my $char (@arr) {
        if ($char eq "]") {
            $add = 0;
            my ($key, $value) = split /=/, $temp_str;
            $temp_str = "";
            $final_hash{$key} = $value;
        }

        if ($add == 1) {
            $temp_str .= $char;
        }

        if ($char eq "[") {
            $add = 1;
        }
    }

    return \%final_hash;
}

# Printea un hash, recibe una referencia al hash
sub print_hash {
