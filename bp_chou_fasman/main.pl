#!/usr/bin/perl

=begin
Bonus point - Implementacion del metodo chou-fasman para la prediccion de estructura secundaria.

Se toman en consideracion las instrucciones del método entregadas en el pdf, y de la página
https://swift.cmbi.umcn.nl/teach/aainfo/chou.shtml
La tabla de puntajes usada es la del pdf

Reglas tomadas de la página:
3 de 5 residuos consecutivos deben tener puntaje beta > 100
Al extender las secuencias a los lados se buscan 4 residuos que en promedio tengan un puntaje < 100,
no 4 consecutivos con puntaje menor que 100.

Se procesa el archivo gbff y se crea un archivo nuevo con las CDS. Este archivo se vuelve a procesar para
hacer la prediccion
Luego de hacer las predicciones se hace un reporte final, con el formato definido
=cut

use strict;
use warnings;
use Bio::SeqIO;

# Recibe la linea "desc" del archivo de traducciones
# La procesa y crea un hash, devuelve la referencia
sub process_desc {
    my $line = shift;
    my @arr = split //, $line;
    my %final_hash;

    my $temp_str = "";
    my $add = 0;

    foreach my $char (@arr) {
        if ($char eq "]") {
            $add = 0;
            my ($key, $value) = split /=/, $temp_str;
            $temp_str = "";
            $final_hash{$key} = $value;
        }

        if ($add == 1) {
            $temp_str .= $char;
        }

        if ($char eq "[") {
            $add = 1;
        }
    }

    return \%final_hash;
}

# Recibe la informacion del archivo de traducciones, devuelve referencias
sub process_translated_cds {
    my $file = shift;
    my $inseq =  Bio::SeqIO->new(-file => $file, -format => "fasta");
    my @data;
    while (my $seq = $inseq->next_seq) {
	my %temp_hash;
	$temp_hash{"display_id"} = $seq->display_id; 
        $temp_hash{"translation"} = $seq->seq;
	$temp_hash{"desc"} = $seq->desc;
        push(@data, \%temp_hash);
    }

    # Arreglo de hashes, cada elemento es una referencia a hash en la que se accede con
    # las keys que estan descritas
    return (\@data);
}

# Procesar el gbff con lo pedido
sub process_gbff{
    my $file = shift;
    my $seqio_object = Bio::SeqIO->new(-file => $file);
    my $seq_object = $seqio_object->next_seq;

    # cada vez que primary tag sea CDS sacar el value del tag protein_id, translation y dbxref,
    # al value de dbxref hace split segun : y usar subindice [1]
    # hacer arreglo de hashes, luego pasar eso a otra funcion que escriba todo en un archivo.

    my @data;
    
    for my $feat_object ($seq_object->get_SeqFeatures){
	my %temp_hash;
	my $p_tag = $feat_object->primary_tag;
	if ($p_tag eq "CDS"){

	    # Sacar valores de los tags
	    for my $tag ($feat_object->get_all_tags){

		# Valor del tag
	        my @values = $feat_object->get_tag_values($tag);
		my $value =  $values[0];
		
		if ($tag eq "db_xref"){
		    my @temp_arr = split /:/, $value;
		    $temp_hash{"gene_id"} = $temp_arr[1];
		    
		}elsif ($tag eq "protein_id"){
		    $temp_hash{$tag} = $value;
		    
		}elsif ($tag eq "translation"){
		    $temp_hash{$tag} = $value;
		}
	    }

	    # Ingresar la location en el diccionario
	    my $start = $feat_object->location->start;
	    my $end = $feat_object->location->end;
	    my $strand = $feat_object->location->strand;
	    my $location = "$start..$end";
	    if ($strand == -1){
		$location = "complement($location)";
	    }
	    
	    $temp_hash{"location"} = $location;

	    # NOTA
	    # Existe una CDS que tiene un geneid y una location asociada, pero que no
	    # tiene translation porque dice que es incompleta
	    # /note="incomplete; partial in the middle of a contig;
            #         missing C-terminus; Derived by automated computational
            #        analysis using gene prediction method: Protein Homology."
	    # por esto se verifica que exista la translation antes de agregar el hash
	    # en este caso esta CDS en especifico se deja fuera
	    # no se puede predecir la estructura secundaria si no se tiene la secuencia

	    # Solo se agrega si se tiene toda la info, no hay nada faltante
	    if (exists $temp_hash{"translation"}){
	        push(@data, \%temp_hash);
	    }
	}
    }
    
    return \@data;
}

# De lista de hashes a texto con cierto formato
sub data_to_text{
    my $data_ref = shift;
    my @data = @$data_ref;
    my $full_text = "";
    # en cada elemento del arreglo esta la referencia a un hash
    foreach my $ref (@data){
	my %temp_hash = %$ref;
	# Con el formato
	# lcl|protein_id|gene [location_product=start..end]
	$full_text = $full_text . ">lcl|$temp_hash{'protein_id'}|$temp_hash{'gene_id'} [location=$temp_hash{'location'}]\n" . "$temp_hash{'translation'}\n";
    }

    return $full_text;
}

# De texto a un archivo dado
sub text_to_file{
    my ($full_text, $file) = @_;

    open(FH, '>', $file) or die $!;
    print FH $full_text;
    close(FH);
}

# Saca el texto de un archivo
sub file_text{
    my $file = shift;
    my $text = "";
    
    open(FH, "<", $file) or die $!;
    while (<FH>){
	$text = $text . $_;
    }
    close (FH);
    return $text;
}

# Recibe la tabla de puntaje de aminoacidos y crea un hash de referencias a hashes
sub process_table{

    # Idea: poner la informacion de la tabla a modo de hash de hashes
    # cada key es el codigo de una letra de cada aminoacido
    # cada value es un hash que tiene todos los valores para ese aminoacido
    # como key se tiene la letra del aminoacido, como value se tiene la referencia a un hash
    # que tiene los keys y values para ese aminoacido en especifico
    
    my $raw_table = shift;
    my @all_lines = split /\n/, $raw_table;
    my %full_hash;
    my @current_line;
    
    my @header = split /\t/, $all_lines[0]; # Header es la primera linea
    
    for (my $i=1; $i < scalar @all_lines; $i++){ # Luego desde la segunda linea en adelante
	my %temp_hash;
	@current_line = split /\t/, $all_lines[$i];
	for (my $j=3; $j < scalar @header; $j++){
	    my $num = sprintf("%.3f", $current_line[$j]);
	    $temp_hash{$header[$j]} = $num;  
	}
	$full_hash{$current_line[2]} = \%temp_hash;
    }

    # Se deja el codigo de tres letras y el nombre completo fuera
    return \%full_hash;
}


sub find_regions{
    my ($cf_ref, $consec, $win_size, $seq_str, $key, $alt_key, $symbol) = @_;
    # ref de tabla chou-fasman, cuantos consecutivos deben ser (3 de 5 o 4 de 6), tamaño de ventana (5 o 6)
    # la secuencia en string, la key es "P(a)" o "P(b)", $symbol es "H" o "B"

    # Arreglo donde cada elemento es un arreglo de dos elementos donde en el primero esta el inicio
    # y el segundo el final de la ventana válida
    my @prediction = [];

    # Se recorre la secuencia
    for(my $i=0; $i<length($seq_str)-$win_size; $i++){
	my $window = substr($seq_str, $i, $win_size); # la ventana a evaluar
	# Verificar que en la ventana hayan $consec elementos con puntaje a o b mayor que 100
	if (check_window($window, $consec, $key, $cf_ref)){
	    # Extender hacia ambos lados hasta encontrar cuatro residuos con puntaje promedio menor que 100
	    # la ventana ahora mismo va desde $i hasta $i+$win_size
	    my $right_side = substr($seq_str, $i+$win_size); # Lado derecho de la ventana
	    my $left_side = reverse substr($seq_str, 0, $i); # Lado izquierdo de la ventana
	    my $add_right = check_extension($right_side, $cf_ref, $key); # Extension de la derecha
	    my $add_left = reverse check_extension($left_side, $cf_ref, $key); # Extension de la izquierda
	    my $segment = $add_left . $window . $right_side; # Segmento final con extensiones añadidad

	    # Al momento de hacer las extensiones de aplica el mismo proceso para el lado izquierdo y derecho,
	    # pero en diferentes sentidos. Se puede invertir el lado izquierdo, procesarlo como si fuera
	    # el lado derecho, y luego volverlo a invertir.

	    # Algunas reglas determinan que se debe verificar el tamaño del segmento en este punto
	    # Se consideran reglas de la pagina y del pdf, la verificacion del tamaño se hace en otro sector
	    
	    my @struc = ($i-length($add_left), $i+$win_size+length($add_right));
	    push (@prediction, \@struc);
	}
    }
    return \@prediction
}

# Revisar si una cierta ventana es valida para trabajar
sub check_window{
    
    my ($window, $consec, $key, $cf_ref) = @_;
    my %cf_hash = %$cf_ref;
    my $streak = 0;
    
    for(my $i=0; $i < length($window); $i++){
	my $aa = substr($window, $i, 1);
	my $data_ref = $cf_hash{$aa};
	my %data = %$data_ref;

	# Se tienen que cumplir $consec residuos consecutivos con puntaje > 100
	if ($streak == $consec){
	    return 1;
	}
	
	if ($data{$key} > 100){
	    $streak++;
	}else{
	    $streak = 0;
	}
    }
    return 0; # Si no se cumple la condicion entonces no es valida
}

# Se extiende la ventana hacia un lado
sub check_extension{
    # ir agregando a una string hasta que se encuentren 4 residuos consecutivos con puntaje
    # menor que 100 promedio
    my ($ext_str, $cf_ref, $key) = @_;
    my $mini_seq = "";
    my %cf_hash = %$cf_ref;
    my $sum = 0;
    my $avg;
    
    for (my $i=0; $i < length($ext_str)-4; $i++){
	my $aa = substr($ext_str, $i, 1);
	my $data_ref = $cf_hash{$aa};
	my %data = %$data_ref;
	
	my $win = substr($ext_str, $i, 4); # Ventana
	for(my $j=0; $j < length($win); $j++){
	    $sum += $data{$key}
	}
	$avg = $sum/4; # Promedio de puntajes dentro de la ventana

	# Si se encuentran se deja de agregar y acaba el trabajo
	if ($avg < 100){
	    return $mini_seq;
	}else{
	    $sum = 0;
	    $mini_seq = $mini_seq . $aa;
	}
    }
    return $mini_seq;
}

# Recibir el arreglo de arreglos con las coordenadas donde empiezan y terminan zonas validas y
# construir una string del estilo:
# ---HHH-----HHHHHH-HHH-H-HHHH-HHHH-HHH-H-H
sub build{
    
    my ($seq_length, $symbol, $pred_ref) = @_;
    my @preds = @$pred_ref;
    my $temp_str = "";

    # Primero se crea una secuencia vacia solo con "-"
    for my $i (0..$seq_length-1){
	$temp_str = $temp_str . "-";
    }

    # Se pasa de string a arreglo
    my @seq = split //, $temp_str;

    # Se recorre el arreglo
    foreach my $el_ref (@preds){
	my @el = @$el_ref;
	if (scalar @el == 0){
	    next;
	}
	# Se reemplaza en el arreglo segun corresponda
	my $start = $el[0];
	my $end = $el[1];
	for(my $j=$start; $j<$end; $j++){
	    @seq[$j] = $symbol;
	}
    }

    # Devolver su referencia. No se procesan largos ni solapamiento con otras estructuras
    return \@seq;
}

# Encontrar los turns
sub find_turns{
    my ($seq_str, $cf_ref) = @_;

    my @prediction;
    my %cf_hash = %$cf_ref;

    # Recorrer la secuencia
    for(my $j=0; $j < length($seq_str)-4; $j++){
	my $win = substr($seq_str, $j, 4); # Ventana
	my @els = split//, $win;

	# Variables para los calculos a hacer
	my $prod_turn = 1; # f(i)*f(i+1)*f(i+2)*f(i+3)
	my $sum_alphas = 0;
	my $sum_betas = 0;
	my $sum_t = 0; #P(t)

	# Verificar la ventana para saber si en el residuo $j hay un turn
	my $n = 0;
	foreach my $el (@els){
	    my $data_ref = $cf_hash{$el};
	    my %data = %$data_ref;
	    my $key;
	    if ($n == 0){
		$key = "f(i)";
	    }else{
		$key = "f(i+$n)";
	    }
	    $prod_turn = $prod_turn * $data{$key}; # Este valor es condicionado
	    $sum_alphas += $data{"P(a)"};
	    $sum_betas += $data{"P(b)"};
	    $sum_t += $data{"P(t)"};
	    $n++
	}

	# Estos otros valores tambien se consideran en la condicion
	my $avg_t = $sum_t/4;
	my $avg_alpha = $sum_alphas/4;
	my $avg_beta = $sum_betas/4;

	# Condicion para saber si en el residuo $j hay turn
	if ($avg_t > 100 && $prod_turn > 0.000075 && $avg_t > $avg_beta && $avg_t > $avg_alpha){
	    push(@prediction, "T");
	}else{
	    push(@prediction, "-");
	}
    }

    # Rellenar los ultimos 4 restantes (se llega el punto en el que quedan 4 residuos, queda un sobrante)
    for my $i (0..3){
	push(@prediction, "-");
    }

    return \@prediction;
}


# Extrae las coordenadas de las regiones donde se solapan las estructuras secundarias
sub get_overlapping{

    my ($alphas_ref, $betas_ref, $seq_str) = @_;

    my @intersections; # Arreglo de arreglos donde estan las coordenadas de intersecciones
    my $started = 0;

    my @alphas = @$alphas_ref;
    my @betas = @$betas_ref;
    my $start;
    my $end;

    # Recorrer la secuencia
    for(my $i=0; $i < length($seq_str); $i++){
	if ($started){
	    if ($alphas[$i] eq "-" || $betas[$i] eq "-"){
		# Se acaba la zona solapada
		$end = $i-1;
		my @inter = ($start, $end);
		push(@intersections, \@inter);
		$started = 0;
	    }
	}else{
	    if ($alphas[$i] ne "-" && $betas[$i] ne "-"){
		# Se entra a una zona solapada
		$start = $i;
		$started = 1;
	    }
	}
    }
    # En caso de que coincidan hasta el final, se ejecuta esta seccion fuera del ciclo
    if ($started){
	$end = length($seq_str)-1; # indices parten del 0
	my @inter = ($start, $end);
	push(@intersections, \@inter);
    }
    
    return \@intersections;
}


# Procesar las intersecciones de modo que se determine cual domina sobre la otra en las regiones solapadas
sub process_intersections{

    my ($seq_str, $cf_ref, $inters_ref) = @_;

    my @dominant; # Arreglo donde se tiene una secuencia en la cual en todas las zonas donde hay interseccion
    # entre Alpha y Beta, se queda con quien domine segun promedio de puntaje en la zona
    my $symbol;
    
    for(my $j=0; $j<length($seq_str); $j++){
	push(@dominant, "-");
    }
    
    my @inters = @$inters_ref;
    my %hash = %$cf_ref;

    my $alpha_sum = 0;
    my $beta_sum = 0;
    
    foreach my $inter_ref (@inters){
	my @inter = @$inter_ref;
	my $n_els = $inter[1] - $inter[0] + 1; # Numero de elementos en la interseccion
	# Ciclar por la cantidad de elementos de la interseccion
	for my $i ($inter[0]..$inter[1]){
	    my $aa = substr($seq_str, $i, 1);
	    my $data_ref = $hash{$aa};
	    my %data = %$data_ref;

	    # Para promedios de alpha y beta
	    $alpha_sum += $data{"P(a)"};
	    $beta_sum += $data{"P(b)"};
	}
	# Promedio de alpha y beta
	my $alpha_avg = $alpha_sum/$n_els;
	my $beta_avg = $beta_sum/$n_els;

	# Dependiendo de quien sea mayor, se determina el dominante
	if ($alpha_avg > $beta_avg){
	    $symbol = "H";
	}else{
	    $symbol = "B";
	}

	# Agregar al arreglo de estructura dominante
	for my $k ($inter[0]..$inter[1]){
	    $dominant[$k] = $symbol;
	}
	$alpha_sum = 0;
	$beta_sum = 0;
    }

    return \@dominant;
}

# Combinar las zonas de Alphas y Betas, para zonas solapadas se escoge la que domine la zona
sub merge_abs{
    # Arreglo solo alpha, solo beta, tabla de puntajes y secuencia original
    my ($alpha_ref, $beta_ref, $cf_ref, $seq_str) = @_;
    my @alphas = @$alpha_ref;
    my @betas = @$beta_ref;
    my $inters_ref = get_overlapping($alpha_ref, $beta_ref, $seq_str); # Sacar intersecciones
    my $dom_ref = process_intersections($seq_str, $cf_ref, $inters_ref);
    my @dominant = @$dom_ref; # Zonas dominantes cuando hay solapamiento

    my @merged; # Secuencia combinada de alpha y beta
    
    for (my $i=0; $i < length($seq_str); $i++){
	if ($alphas[$i] eq $betas[$i]){ # Si alpha y beta tienen el mismo elemento, es "-" necesariamente
	    push(@merged, "-");
	}else{
	    if ($alphas[$i] eq "-"){ # Si alpha solamente tiene "-", se agrega el elemento de beta
		push(@merged, $betas[$i]);
	    }elsif($betas[$i] eq "-"){ # Si es al reves, se agrega el elemento de alpha
		push(@merged, $alphas[$i]);
	    }else{ # Si no, hay "H" em alpha y "B" em beta, se escoge quien domine
		push(@merged, $dominant[$i]);
	    }
	}
    }

    
    return \@merged;
}

# Insertar los turns en la mezcla de alphas y betas, tienen prioridad sobre estas, siempre
sub insert_turns{
    my ($merged_ab_ref, $turns_ref) = @_;

    my @merged_ab = @$merged_ab_ref;
    my @turns = @$turns_ref;

    my @result;
    
    for(my $i=0; $i < scalar @turns; $i++){

	# Si hay turn se inserta
	if ($turns[$i] ne "-"){
	    push(@result, $turns[$i]);
	}else{ # Si no se inserta lo que haya en la mezcla de alphas y betas
	    push(@result, $merged_ab[$i]);
	}
    }

    return \@result;
}

# Se revisa el largo de las estructuras secundarias predichas
# Alpha helice y lamina beta debe tener más de 5 residuos
# Turns no hay largo minimo para ser valido
sub check_struc_length{
    
    my $seq_ref = shift;

    my @seq = @$seq_ref;

    my $current;
    my $started;
    
    my @group;
    my @final;
    
    $started = 0;
    foreach my $aa (@seq){
	if ($started){
	    if ($aa eq $current){
		# Si se sigue en esa estructura se agrega al arreglo group
		push(@group, $aa);
	    }else{
		# Cuando se acaba la estructura se revisa su largo para verificar si es valida o no
		if (scalar @group < 5 && $current ne "T"){ # Caso de largo menor que 5, no es valida
		    foreach my $el (@group){
			push(@final, "-");
		    }
		}else{
		    foreach my $el (@group){
			push(@final, $el); # Caso de largo mayor que 5, es valida
		    }
		}

		# Limpiar group y otras variables para siguiente estructura
		@group = ();	
		$current = $aa;
		push(@group, $aa);
	    }
	}else{
	    # Cuando empieza una estructura se empieza a contar su largo
	    $current = $aa;
	    push(@group, $aa);
	    $started = 1;
	}
    }

    # Verificar en caso de que aun queden elementos en el grupo luego de terminar el ciclo
    if (scalar @group > 5){
	foreach my $el (@group){
	    push(@final, $el);
	}
    }else{
	foreach my $el (@group){
	    push(@final, "-");
	}
    }

    return \@final;
}

# Reemplaza un elemento por otro en un arreglo, todas las coincidencias
sub replace{
    my ($arr_ref, $replaced, $new_el) = @_;
    my @arr = @$arr_ref;
    my @final;
    
    foreach my $el (@arr){
	if ($el eq $replaced){
	    push(@final, $new_el)
	}else{
	    push(@final, $el);
	}
    }

    return \@final;
}

# Algortimo Chou-Fasman
sub chou_fasman{
    my ($seq_str, $cf_ref) = @_;
    
    # Se sabe el puntaje de cada uno de los aminoacidos via tabla

    # Buscar Alpha helice
    my $alphas_ref = find_regions($cf_ref, 4, 6, $seq_str, "P(a)", "P(b)", "H");
    my $alphas_build_ref = build(length($seq_str), "H", $alphas_ref);

    # Buscar laminas beta
    my $betas_ref = find_regions($cf_ref, 3, 5, $seq_str, "P(b)", "P(a)", "B");
    my $betas_build_ref = build(length($seq_str), "B", $betas_ref);

    # Buscar los turns
    my $turns_ref = find_turns($seq_str, $cf_ref);

    # Fusionar alphas y betas considerando dominio de una sobre la otra segun corresponda
    my $merged_ab_ref = merge_abs($alphas_build_ref, $betas_build_ref, $cf_ref, $seq_str);

    # Se agregar los turns
    my $final_unchecked_ref = insert_turns($merged_ab_ref, $turns_ref);
    # Se revisa el largo de cada estructura secundaria predicha
    my $final_nocoil_ref = check_struc_length($final_unchecked_ref);
    # Reemplazar cada "-" por "C" para los coils
    my $final_ref = replace($final_nocoil_ref, "-", "C");
    my @final = @$final_ref;

    return \@final;
}

# Hacer el texto del reporte final
sub make_final_report{

    my ($sec_strucs_ref, $cf_ref, $data_trans_ref) = @_;
    # Se tienen tantas predicciones de estructura secundaria como secuencias, o
    # como elementos dentro de $data_trans_ref

    # Arreglo con las estructuras secundarias
    # cada elemento es una referencia a un arreglo en el cual en cada indice se tiene la letra de la estructura secundaria
    my @sec_strucs = @$sec_strucs_ref;
    # Arreglo con la informacion de una secuencia
    # cada elemento es una referencia a un hash en el cual se tienen las keys de disply_id, desc y translation
    my @data = @$data_trans_ref;

    my %cf_hash = %$cf_ref; # Tabla de chou-fasman

    my $text = "";

    # Para cada secuencia aminoacidica se construye el reporte
    for(my $i=0; $i < scalar @sec_strucs; $i++){
	# Extraer la informacion
	my $seq_hash_ref = $data[$i];
	my %seq_hash = %$seq_hash_ref;
	my $seq_str = $seq_hash{"translation"};
	my @seq = split//, $seq_str;
	my $second_ref = $sec_strucs[$i];
	my @second_struc = @$second_ref;

	# "Header" para cada proteina
	$text = $text . ">$seq_hash{\"display_id\"} $seq_hash{\"desc\"}\n";

	# Determinar el puntaje asociado a cada prediccion
	for(my $j=0; $j < scalar @seq; $j++){
	    my $aa = $seq[$j];
	    my $struc = $second_struc[$j];
	    my $aa_hash_ref = $cf_hash{$aa};
	    my %aa_hash = %$aa_hash_ref;

	    my $score;
	    
	    if ($struc eq "H"){
		$score = $aa_hash{"P(a)"};
	    }elsif($struc eq "B"){
		$score = $aa_hash{"P(b)"};
	    }elsif($struc eq "T"){
		$score = $aa_hash{"P(t)"};
	    }else{
		$score = "-";
	    }

	    # Agregar cada otra linea abajo del header, asociado a la misma proteina
	    $text = $text . "$j\t$aa\t$struc\t$score\n";
	}
    }

    return $text;
}

sub main {
    # Archivo gbff inicial
    my $filename = "genbank_file.gbff";
    # Extraer la informacion necesaria del gbff
    my $data_ref = process_gbff($filename);
    # La info extraida se pasa a texto con formato requerido
    my $text = data_to_text($data_ref);
    # Guardar el texto un archivo
    my $trans_file = "my_translated_cds.fasta";
    text_to_file($text, $trans_file);
    # Extraer la informacion de este nuevo archivo
    my $data_trans_ref = process_translated_cds("my_translated_cds.fasta");
    # Extraer informacion de la tabla con puntajes
    my $aa_table = file_text("chou-fasman_table.txt");
    my $cf_ref = process_table($aa_table);

    # Para cada secuencia hacer su prediccion de estructura secundaria, guardar la prediccion en un arreglo
    my @sec_strucs;
    foreach my $ref (@$data_trans_ref){
	my %hash = %$ref;
	push(@sec_strucs, chou_fasman($hash{"translation"}, $cf_ref));
    }

    # Hacer el reporte final y guardarlo en un archivo
    my $final_text = make_final_report(\@sec_strucs, $cf_ref, $data_trans_ref);
    text_to_file($final_text, "final_report.txt");
    
}

main()
