# Se pone a prueba el algoritmo Chou-Fasman desarrollado
# Se tiene una carpeta con multiples archivos PDB.
# por cada uno de estos, se compara la estructura secundaria determinada por Stride con la determinada
# por Chou-Fasman.
# Se calcula el porcentaje de identidad, verdaderos y falsos positivos y negativos

use strict;
use warnings;

use Chart::Gnuplot;


# De texto a un archivo dado
sub text_to_file{
    my ($full_text, $file) = @_;

    open(FH, '>', $file) or die $!;
    print FH $full_text;
    close(FH);
}

# Saca el texto de un archivo
sub file_text{
    my $file = shift;
    my $text = "";
    
    open(FH, "<", $file) or die $!;
    while (<FH>){
	$text = $text . $_;
    }
    close (FH);
    return $text;
}

# Recibe la tabla de puntaje de aminoacidos y crea un hash de referencias a hashes
sub process_table{

    # Idea: poner la informacion de la tabla a modo de hash de hashes
    # cada key es el codigo de una letra de cada aminoacido
    # cada value es un hash que tiene todos los valores para ese aminoacido
    # como key se tiene la letra del aminoacido, como value se tiene la referencia a un hash
    # que tiene los keys y values para ese aminoacido en especifico
    
    my $raw_table = shift;
    my @all_lines = split /\n/, $raw_table;
    my %full_hash;
    my @current_line;
    
    my @header = split /\t/, $all_lines[0]; # Header es la primera linea
    
    for (my $i=1; $i < scalar @all_lines; $i++){ # Luego desde la segunda linea en adelante
	my %temp_hash;
	@current_line = split /\t/, $all_lines[$i];
	for (my $j=3; $j < scalar @header; $j++){
	    my $num = sprintf("%.3f", $current_line[$j]);
	    $temp_hash{$header[$j]} = $num;  
	}
	$full_hash{$current_line[2]} = \%temp_hash;
    }

    # Se deja el codigo de tres letras y el nombre completo fuera
    return \%full_hash;
}


sub find_regions{
    my ($cf_ref, $consec, $win_size, $seq_str, $key, $alt_key, $symbol) = @_;
    # ref de tabla chou-fasman, cuantos consecutivos deben ser (3 de 5 o 4 de 6), tamaño de ventana (5 o 6)
    # la secuencia en string, la key es "P(a)" o "P(b)", $symbol es "H" o "B"

    # Arreglo donde cada elemento es un arreglo de dos elementos donde en el primero esta el inicio
    # y el segundo el final de la ventana válida
    my @prediction = [];

    # Se recorre la secuencia
    for(my $i=0; $i<length($seq_str)-$win_size; $i++){
	my $window = substr($seq_str, $i, $win_size); # la ventana a evaluar
	# Verificar que en la ventana hayan $consec elementos con puntaje a o b mayor que 100
	if (check_window($window, $consec, $key, $cf_ref)){
	    # Extender hacia ambos lados hasta encontrar cuatro residuos con puntaje promedio menor que 100
	    # la ventana ahora mismo va desde $i hasta $i+$win_size
	    my $right_side = substr($seq_str, $i+$win_size); # Lado derecho de la ventana
	    my $left_side = reverse substr($seq_str, 0, $i); # Lado izquierdo de la ventana
	    my $add_right = check_extension($right_side, $cf_ref, $key); # Extension de la derecha
	    my $add_left = reverse check_extension($left_side, $cf_ref, $key); # Extension de la izquierda
	    my $segment = $add_left . $window . $right_side; # Segmento final con extensiones añadidad

	    # Al momento de hacer las extensiones de aplica el mismo proceso para el lado izquierdo y derecho,
	    # pero en diferentes sentidos. Se puede invertir el lado izquierdo, procesarlo como si fuera
	    # el lado derecho, y luego volverlo a invertir.

	    # Algunas reglas determinan que se debe verificar el tamaño del segmento en este punto
	    # Se consideran reglas de la pagina y del pdf, la verificacion del tamaño se hace en otro sector
	    
	    my @struc = ($i-length($add_left), $i+$win_size+length($add_right));
	    push (@prediction, \@struc);
	}
    }
    return \@prediction
}

# Revisar si una cierta ventana es valida para trabajar
sub check_window{
    
    my ($window, $consec, $key, $cf_ref) = @_;
    my %cf_hash = %$cf_ref;
    my $streak = 0;
    
    for(my $i=0; $i < length($window); $i++){
	my $aa = substr($window, $i, 1);
	my $data_ref = $cf_hash{$aa};
	my %data = %$data_ref;

	# Se tienen que cumplir $consec residuos consecutivos con puntaje > 100
	if ($streak == $consec){
	    return 1;
	}
	
	if ($data{$key} > 100){
	    $streak++;
	}else{
	    $streak = 0;
	}
    }
    return 0; # Si no se cumple la condicion entonces no es valida
}

# Se extiende la ventana hacia un lado
sub check_extension{
    # ir agregando a una string hasta que se encuentren 4 residuos consecutivos con puntaje
    # menor que 100 promedio
    my ($ext_str, $cf_ref, $key) = @_;
    my $mini_seq = "";
    my %cf_hash = %$cf_ref;
    my $sum = 0;
    my $avg;
    
    for (my $i=0; $i < length($ext_str)-4; $i++){
	my $aa = substr($ext_str, $i, 1);
	my $data_ref = $cf_hash{$aa};
	my %data = %$data_ref;
	
	my $win = substr($ext_str, $i, 4); # Ventana
	for(my $j=0; $j < length($win); $j++){
	    $sum += $data{$key}
	}
	$avg = $sum/4; # Promedio de puntajes dentro de la ventana

	# Si se encuentran se deja de agregar y acaba el trabajo
	if ($avg < 100){
	    return $mini_seq;
	}else{
	    $sum = 0;
	    $mini_seq = $mini_seq . $aa;
	}
    }
    return $mini_seq;
}

# Recibir el arreglo de arreglos con las coordenadas donde empiezan y terminan zonas validas y
# construir una string del estilo:
# ---HHH-----HHHHHH-HHH-H-HHHH-HHHH-HHH-H-H
sub build{
    
    my ($seq_length, $symbol, $pred_ref) = @_;
    my @preds = @$pred_ref;
    my $temp_str = "";

    # Primero se crea una secuencia vacia solo con "-"
    for my $i (0..$seq_length-1){
	$temp_str = $temp_str . "-";
    }

    # Se pasa de string a arreglo
    my @seq = split //, $temp_str;

    # Se recorre el arreglo
    foreach my $el_ref (@preds){
	my @el = @$el_ref;
	if (scalar @el == 0){
	    next;
	}
	# Se reemplaza en el arreglo segun corresponda
	my $start = $el[0];
	my $end = $el[1];
	for(my $j=$start; $j<$end; $j++){
	    @seq[$j] = $symbol;
	}
    }

    # Devolver su referencia. No se procesan largos ni solapamiento con otras estructuras
    return \@seq;
}

# Encontrar los turns
sub find_turns{
    my ($seq_str, $cf_ref) = @_;

    my @prediction;
    my %cf_hash = %$cf_ref;

    # Recorrer la secuencia
    for(my $j=0; $j < length($seq_str)-4; $j++){
	my $win = substr($seq_str, $j, 4); # Ventana
	my @els = split//, $win;

	# Variables para los calculos a hacer
	my $prod_turn = 1; # f(i)*f(i+1)*f(i+2)*f(i+3)
	my $sum_alphas = 0;
	my $sum_betas = 0;
	my $sum_t = 0; #P(t)

	# Verificar la ventana para saber si en el residuo $j hay un turn
	my $n = 0;
	foreach my $el (@els){
	    my $data_ref = $cf_hash{$el};
	    my %data = %$data_ref;
	    my $key;
	    if ($n == 0){
		$key = "f(i)";
	    }else{
		$key = "f(i+$n)";
	    }
	    $prod_turn = $prod_turn * $data{$key}; # Este valor es condicionado
	    $sum_alphas += $data{"P(a)"};
	    $sum_betas += $data{"P(b)"};
	    $sum_t += $data{"P(t)"};
	    $n++
	}

	# Estos otros valores tambien se consideran en la condicion
	my $avg_t = $sum_t/4;
	my $avg_alpha = $sum_alphas/4;
	my $avg_beta = $sum_betas/4;

	# Condicion para saber si en el residuo $j hay turn
	if ($avg_t > 100 && $prod_turn > 0.000075 && $avg_t > $avg_beta && $avg_t > $avg_alpha){
	    push(@prediction, "T");
	}else{
	    push(@prediction, "-");
	}
    }

    # Rellenar los ultimos 4 restantes (se llega el punto en el que quedan 4 residuos, queda un sobrante)
    for my $i (0..3){
	push(@prediction, "-");
    }

    return \@prediction;
}


# Extrae las coordenadas de las regiones donde se solapan las estructuras secundarias
sub get_overlapping{

    my ($alphas_ref, $betas_ref, $seq_str) = @_;

    my @intersections; # Arreglo de arreglos donde estan las coordenadas de intersecciones
    my $started = 0;

    my @alphas = @$alphas_ref;
    my @betas = @$betas_ref;
    my $start;
    my $end;

    # Recorrer la secuencia
    for(my $i=0; $i < length($seq_str); $i++){
	if ($started){
	    if ($alphas[$i] eq "-" || $betas[$i] eq "-"){
		# Se acaba la zona solapada
		$end = $i-1;
		my @inter = ($start, $end);
		push(@intersections, \@inter);
		$started = 0;
	    }
	}else{
	    if ($alphas[$i] ne "-" && $betas[$i] ne "-"){
		# Se entra a una zona solapada
		$start = $i;
		$started = 1;
	    }
	}
    }
    # En caso de que coincidan hasta el final, se ejecuta esta seccion fuera del ciclo
    if ($started){
	$end = length($seq_str)-1; # indices parten del 0
	my @inter = ($start, $end);
	push(@intersections, \@inter);
    }
    
    return \@intersections;
}


# Procesar las intersecciones de modo que se determine cual domina sobre la otra en las regiones solapadas
sub process_intersections{

    my ($seq_str, $cf_ref, $inters_ref) = @_;

    my @dominant; # Arreglo donde se tiene una secuencia en la cual en todas las zonas donde hay interseccion
    # entre Alpha y Beta, se queda con quien domine segun promedio de puntaje en la zona
    my $symbol;
    
    for(my $j=0; $j<length($seq_str); $j++){
	push(@dominant, "-");
    }
    
    my @inters = @$inters_ref;
    my %hash = %$cf_ref;

    my $alpha_sum = 0;
    my $beta_sum = 0;
    
    foreach my $inter_ref (@inters){
	my @inter = @$inter_ref;
	my $n_els = $inter[1] - $inter[0] + 1; # Numero de elementos en la interseccion
	# Ciclar por la cantidad de elementos de la interseccion
	for my $i ($inter[0]..$inter[1]){
	    my $aa = substr($seq_str, $i, 1);
	    my $data_ref = $hash{$aa};
	    my %data = %$data_ref;

	    # Para promedios de alpha y beta
	    $alpha_sum += $data{"P(a)"};
	    $beta_sum += $data{"P(b)"};
	}
	# Promedio de alpha y beta
	my $alpha_avg = $alpha_sum/$n_els;
	my $beta_avg = $beta_sum/$n_els;

	# Dependiendo de quien sea mayor, se determina el dominante
	if ($alpha_avg > $beta_avg){
	    $symbol = "H";
	}else{
	    $symbol = "E";
	}

	# Agregar al arreglo de estructura dominante
	for my $k ($inter[0]..$inter[1]){
	    $dominant[$k] = $symbol;
	}
	$alpha_sum = 0;
	$beta_sum = 0;
    }

    return \@dominant;
}

# Combinar las zonas de Alphas y Betas, para zonas solapadas se escoge la que domine la zona
sub merge_abs{
    # Arreglo solo alpha, solo beta, tabla de puntajes y secuencia original
    my ($alpha_ref, $beta_ref, $cf_ref, $seq_str) = @_;
    my @alphas = @$alpha_ref;
    my @betas = @$beta_ref;
    my $inters_ref = get_overlapping($alpha_ref, $beta_ref, $seq_str); # Sacar intersecciones
    my $dom_ref = process_intersections($seq_str, $cf_ref, $inters_ref);
    my @dominant = @$dom_ref; # Zonas dominantes cuando hay solapamiento

    my @merged; # Secuencia combinada de alpha y beta
    
    for (my $i=0; $i < length($seq_str); $i++){
	if ($alphas[$i] eq $betas[$i]){ # Si alpha y beta tienen el mismo elemento, es "-" necesariamente
	    push(@merged, "-");
	}else{
	    if ($alphas[$i] eq "-"){ # Si alpha solamente tiene "-", se agrega el elemento de beta
		push(@merged, $betas[$i]);
	    }elsif($betas[$i] eq "-"){ # Si es al reves, se agrega el elemento de alpha
		push(@merged, $alphas[$i]);
	    }else{ # Si no, hay "H" em alpha y "B" em beta, se escoge quien domine
		push(@merged, $dominant[$i]);
	    }
	}
    }

    
    return \@merged;
}

# Insertar los turns en la mezcla de alphas y betas, tienen prioridad sobre estas, siempre
sub insert_turns{
    my ($merged_ab_ref, $turns_ref) = @_;

    my @merged_ab = @$merged_ab_ref;
    my @turns = @$turns_ref;

    my @result;
    
    for(my $i=0; $i < scalar @turns; $i++){

	# Si hay turn se inserta
	if ($turns[$i] ne "-"){
	    push(@result, $turns[$i]);
	}else{ # Si no se inserta lo que haya en la mezcla de alphas y betas
	    push(@result, $merged_ab[$i]);
	}
    }

    return \@result;
}

# Se revisa el largo de las estructuras secundarias predichas
# Alpha helice y lamina beta debe tener más de 5 residuos
# Turns no hay largo minimo para ser valido
sub check_struc_length{
    
    my $seq_ref = shift;

    my @seq = @$seq_ref;

    my $current;
    my $started;
    
    my @group;
    my @final;
    
    $started = 0;
    foreach my $aa (@seq){
	if ($started){
	    if ($aa eq $current){
		# Si se sigue en esa estructura se agrega al arreglo group
		push(@group, $aa);
	    }else{
		# Cuando se acaba la estructura se revisa su largo para verificar si es valida o no
		if (scalar @group < 5 && $current ne "T"){ # Caso de largo menor que 5, no es valida
		    foreach my $el (@group){
			push(@final, "-");
		    }
		}else{
		    foreach my $el (@group){
			push(@final, $el); # Caso de largo mayor que 5, es valida
		    }
		}

		# Limpiar group y otras variables para siguiente estructura
		@group = ();	
		$current = $aa;
		push(@group, $aa);
	    }
	}else{
	    # Cuando empieza una estructura se empieza a contar su largo
	    $current = $aa;
	    push(@group, $aa);
	    $started = 1;
	}
    }

    # Verificar en caso de que aun queden elementos en el grupo luego de terminar el ciclo
    if (scalar @group > 5){
	foreach my $el (@group){
	    push(@final, $el);
	}
    }else{
	foreach my $el (@group){
	    push(@final, "-");
	}
    }

    return \@final;
}

# Reemplaza un elemento por otro en un arreglo, todas las coincidencias
sub replace{
    my ($arr_ref, $replaced, $new_el) = @_;
    my @arr = @$arr_ref;
    my @final;
    
    foreach my $el (@arr){
	if ($el eq $replaced){
	    push(@final, $new_el)
	}else{
	    push(@final, $el);
	}
    }

    return \@final;
}

# Algortimo Chou-Fasman
sub chou_fasman{
    my ($seq_str, $cf_ref) = @_;
    
    # Se sabe el puntaje de cada uno de los aminoacidos via tabla

    # Buscar Alpha helice
    my $alphas_ref = find_regions($cf_ref, 4, 6, $seq_str, "P(a)", "P(b)", "H");
    my $alphas_build_ref = build(length($seq_str), "H", $alphas_ref);

    # Buscar laminas beta
    my $betas_ref = find_regions($cf_ref, 3, 5, $seq_str, "P(b)", "P(a)", "E");
    my $betas_build_ref = build(length($seq_str), "E", $betas_ref);

    # Buscar los turns
    my $turns_ref = find_turns($seq_str, $cf_ref);

    # Fusionar alphas y betas considerando dominio de una sobre la otra segun corresponda
    my $merged_ab_ref = merge_abs($alphas_build_ref, $betas_build_ref, $cf_ref, $seq_str);

    # Se agregar los turns
    my $final_unchecked_ref = insert_turns($merged_ab_ref, $turns_ref);
    # Se revisa el largo de cada estructura secundaria predicha
    my $final_nocoil_ref = check_struc_length($final_unchecked_ref);
    # Reemplazar cada "-" por "C" para los coils
    my $final_ref = replace($final_nocoil_ref, "-", "C");
    my @final = @$final_ref;

    return \@final;
}

# ELEMENTOS NUEVOS DE ACA EN ADELANTE

# Listar elementos de un directorio, devolver referencia a la lista.
sub list_dir{
    my $path = shift;
    opendir my $dir, $path or die "No se pudo abrir el directorio: $!";
    my @raw = readdir $dir;
    my @files;
    foreach my $el (@raw){
	if ($el ne "." && $el ne ".."){
	    push(@files, $el);
	} 
    }
    closedir $dir;
    return \@files;
}


# Por cada uno de los archivos en la carpeta, se ejecuta el comando de Stride, se procesa la salida
# de modo que se devuelva un hash, este hash se ingresa en otro hash, siendo el value de una key representada
# por el nombre del archivo mismo.
sub get_real_data{
    # Listar los elementos de la carpeta de proteinas
    my $path = "/home/seba/Documents/analisis_de_secuencias_biologicas/bp_chou_fasman/proteins";
    my $files_ref = list_dir($path);
    my @files = @$files_ref;

    # Por cada archivo, usar Stride y procesar la salida, guardarla en un hash
    my %full_hash;
    foreach my $file (@files){
	#print($file, "\n\n");
	# sacar la data del archivo
	# poner la data del archivo junto al nombre del archivo en el hash
	my $raw_data = `stride -o proteins/$file`;
	$full_hash{$file} = process_stride_output($raw_data);
    }

    # Retornar la refrencia al hash con toda la información
    return \%full_hash;
}

# Procesar la salida al usar stride
# En la documentación está especificado que las lineas que empiezan con SEQ corresponden
# a la secuencia aminoacidica.
# STR para la estructura secundaria.
# para ambos casos, la secuencia va de la posicion 11 a 60 (Largo de 50 para substr)
# Se crea un hash de hashes donde cada key es el nombre de la cadena, el value es un hash
# donde se tienen keys y values para: la secuencia aminoacidica y la predicha por Stride (la real,
sub process_stride_output{

    # Dividir la salida de Stride en lineas
    my $stride_text = shift;
    my @lines = split /\n/, $stride_text;

    # Hash para la informacion
    my %full_info;

    # Alfabeto para el nombre de las cadenas
    my @alphabet = ("A".."Z");

    # Variables a usar dentro del loop
    my $chain_counter = 0;
    my $seq = ""; # secuencia de aminoacidos
    my $str = ""; # estructura de Stride
    my @str_as_arr;

    # Recorrer cada una de las lineas
    foreach my $line (@lines){

	my $rec_code = substr($line, 0, 3); # Primeras tres letras de la linea
	# Cada vez que se encuentra una cadena, hacer el cambio
	if ($rec_code eq "CHN"){
	    if ($seq ne "" && $str ne ""){ # Importante para el primer encuentro de cadena, pues en ese caso $seq y $str aun serán "";
		# Informacion de la cadena
		my %chain_info;
		$chain_info{"seq"} = $seq;


		# La estructura secundaria puede ser "Mas corta" que la secuencia real, pues
		# Pueden haber caracteres espacios en la estructura secundaria, al ser representados los Coils con
		# un espacio en vez de C. Por esto siempre que se encuentra una str se reemplazan los espacios por C.
		# Sin embargo para el ultimo trecho de estructura (y de secuencia tambien) puede haber espacios vacios en caso
		# de que no se ocupe "todo el espacio" de la substring con caracteres.
		$str = substr($str, 0, length($seq));

		# Agregar la info al diccionario temporal y final
		$chain_info{"real_str"} = $str;
		$full_info{"chain $alphabet[$chain_counter]"} = \%chain_info;

		# Resetar para el siguiente encuentro
		$seq = "";
		$str = "";
		$chain_counter++; # Aumentar para pasar de "A" a "B" y asi sucesivamente
	    }
	# Agregar info de la secuencia aminoacidica
	}elsif ($rec_code eq "SEQ"){
	    my $added = substr($line, 10, 50);
	    $added =~ s/ //g; # Eliminar espacios vacios (del ultimo trecho)
	    $seq = $seq . $added;
	# agregar info de la estructura secundaria
	}elsif ($rec_code eq "STR"){
	    my $added = substr($line, 10, 50);
	    $added =~ s/ /C/g; # Cambiar espacios vacios por C
	    $str = $str . $added;
	}
    }

    # Para este punto, se tiene la informacion de la ultima cadena, pero no se ha agregado porque no se volvio a encontrar "CHN"
    # en las primeras tres letras. Se hace un procesamiento más, fuera del loop, para agregarla
    $str = substr($str, 0, length($seq));
    my %chain_info;
    $chain_info{"seq"} = $seq;
    $chain_info{"real_str"} = $str;
    $full_info{"chain $alphabet[$chain_counter]"} = \%chain_info; # No se descuenta 1 al contador, pues es la ultima secuencia

    return \%full_info
}

# Hace la prediccion de cada estructura, real vs predicha, y calcula:
# Porcentaje de identidad entre la original y la predicha
# Verdaderos positivos: Match (igual que pid)
# Verdaderos negativos: Str vs coil
# Falsos positivos: Str1 != Str2
# Falsos negativos: C vs str
sub make_full_data{
    
    # recibir la tabla chou-fasman y la data
    # chou-fasman recibe una string y devuelve la referencia a un arreglo donde cada elemento es el aminoacido predicho
    my ($cf_ref, $data_ref) = @_;
    my %data = %$data_ref;

    # Recorrer el hash de hashes con la informacion
    foreach my $protein (keys %data){
	#print("Trabajando con $protein\n");
	my $chains_ref = $data{$protein};
	my %chains = %$chains_ref; # Hash con donde cada key es "chain A" o B o C...
	foreach my $chain (keys %chains){
	    my $chn_data_ref = $chains{$chain};
	    my %chn_data = %$chn_data_ref; # Hash donde se tiene la secuencia y la estructura de esa cadena

	    # Aplicar chou-fasman
	    my $cf_pred_ref = chou_fasman($chn_data{"seq"}, $cf_ref);
	    my @cf_pred = @$cf_pred_ref;
	    my $pred_str = join("", @cf_pred);

	    # Agregar prediccion a la informacion de la cadena
	    $chn_data{"predicted"} = $pred_str;
	    
	    my $conf_alpha_ref = make_conf_table($chn_data{"predicted"}, $chn_data{"real_str"}, "H");
	    $chn_data{"conf_alpha"} = $conf_alpha_ref;
	    
	    my $conf_beta_ref = make_conf_table($chn_data{"predicted"}, $chn_data{"real_str"}, "E");
	    $chn_data{"conf_beta"} = $conf_beta_ref;
	    
	    my $conf_turn_ref = make_conf_table($chn_data{"predicted"}, $chn_data{"real_str"}, "T");
	    $chn_data{"conf_turn"} = $conf_turn_ref;
	    
	    # Hacer la comparacion real-predicha
	    #my $analysis_ref = analyze($chn_data{"real_str"}, $chn_data{"predicted"});
	    #my %analysis = %$analysis_ref;
	    # Agregar el nuevo hash al original
	    #%chn_data = (%chn_data, %analysis);
	    # Actualizar el hash para esa cadena
	    $chains{$chain} = \%chn_data;
	}
	# Actualizar el hash, pero para la proteina
	$data{$protein} = \%chains;
    }
    return \%data;
}

# hacer la tabla de confusion para una estructura secundaria
# se tiene que recibir la secuencia real y la predicha, y armar un diccionario donde se tenga la tabla
# de confusion, en cada key se tienen los TP TN y asi, y en el value el conteo
# luego con esa informacion se puede hacer la tp_rate y la fp_rate
# con esos dos valores se arma el punto (fp_date, tp_rate) y asi con todas las chains se puede armar el grafico ROC

sub make_conf_table{
    my ($pred_str, $real_str, $symbol) = @_;
    my $tp = 0;
    my $tn = 0;
    my $fp = 0;
    my $fn = 0;

    my %conf_table;
    
    for (my $i=0; $i < length($pred_str); $i++){
	my $pred_struct = substr($pred_str, $i, 1);
	my $real_struct = substr($real_str, $i, 1);

	# si real es positivo y pred es positivo, tp
	if ($real_struct eq $symbol && $pred_struct eq $symbol){
	    $tp = $tp + 1;
	}elsif($real_struct eq $symbol && $pred_struct ne $symbol){
	    $fn = $fn + 1;
	} elsif ($real_struct ne $symbol && $pred_struct eq $symbol){
	    $fp = $fp + 1;
	}elsif ($real_struct ne $symbol && $pred_struct ne $symbol){
	    $tn = $tn + 1;
	}
    }

    $conf_table{"tp"} = $tp;
    $conf_table{"tn"} = $tn;
    $conf_table{"fp"} = $fp;
    $conf_table{"fn"} = $fn;

    return \%conf_table;
}

# Mostrar la informacion del hash de hashes de hashes
sub print_data{
    my $data_ref = shift;
    my %data = %$data_ref;

    foreach my $protein (keys %data){
	print($protein, "\n");
	my $chains_ref = $data{$protein};
	my %chains = %$chains_ref;
	foreach my $chain (keys %chains){
	    my $chn_data_ref = $chains{$chain};
	    my %chn_data = %$chn_data_ref;
	    print("\t", $chain, "\n");
	    print("\t\tor:\t$chn_data{'real_str'}\n");
	    print("\t\tpr:\t$chn_data{'predicted'}\n");
	    #my $conf_beta_ref = $chn_data{"conf_beta"};
	    #my %conf_beta = %$conf_beta_ref;
	    #foreach my $key (keys %conf_beta){
		#print("\t\t$key:\t$conf_beta{$key}\n");
	    #}
	    
	    #print("$chn_data{"pid"}\”");
	    #foreach my $key (keys %chn_data){
		#print("$key: $chn_data{$key}\n\n");
	    #}
	}
    }
}

# Recibir la data total y si se trabajara con alphas betas o turns
# por cada chain sacar el par (fp_rate, tp_rate), agregar el cada uno a un arreglo
sub make_axis{

    my ($data_ref, $working_hash) = @_;
    my %data = %$data_ref;

    my @fp_rates;
    my @tp_rates;
    
    my @final;

    foreach my $protein (keys %data){
	my $chains_ref = $data{$protein};
	my %chains = %$chains_ref;

	foreach my $chain (keys %chains){
	    my $chn_data_ref = $chains{$chain};
	    my %chn_data = %$chn_data_ref;

	    my $conf_ref = $chn_data{$working_hash};
	    my %conf_table = %$conf_ref;

	    #print($conf_table{"tp"}, "\n");
	    #print($conf_table{"fp"}, "\n");
	    #print($conf_table{"tn"}, "\n");
	    #print($conf_table{"fn"}, "\n\n");
	    
	    my $tp_rate = $conf_table{"tp"}/($conf_table{"tp"} + $conf_table{"fn"} + 0.0000001);
	    my $fp_rate = $conf_table{"fp"}/($conf_table{"fp"} + $conf_table{"tn"} + 0.0000001);

	    push(@fp_rates, $fp_rate);
	    push(@tp_rates, $tp_rate);
	}
    }
    
    push(@final, \@fp_rates);
    push(@final, \@tp_rates);

    return \@final;
}

# recibe un arreglo con los dos ejes, hace el grafico
sub graphic_data{

    my ($data_ref, $name) = @_; #beta_data, alpha_data o turn_data
    my @data = @$data_ref;

    my $fp_rates_ref = $data[0];
    my $tp_rates_ref = $data[1];

    my $chart = Chart::Gnuplot->new(
	output => "grafico_$name.png",
	title => "ROC curve",
	xlabel => "False positive rate",
	ylabel => "True positive rate",
    );

    my $dataSet = Chart::Gnuplot::DataSet->new(

	xdata => $fp_rates_ref,
	ydata => $tp_rates_ref,
	style => "lines", # modalidad del grafico, lineas o puntos por ejemplo
	title => "***",

    );

    $chart->plot2d($dataSet);
    #$chart->export("scatter_plot.png");
}


sub make_table{
    my ($data_ref, $name) = @_;
    my @data = @$data_ref;

    my $fp_rates_ref = $data[0];
    my $tp_rates_ref = $data[1];

    my @fp_rates = @$fp_rates_ref;
    my @tp_rates = @$tp_rates_ref;


    my @specificity;
    foreach my $fp (@fp_rates){
	my $val = 1 - $fp;
	push(@specificity, $val);
    }
    # sensitivity = recall = tp_rate, asi que no se modifica

    my $full_text = "";
    for (my $i = 0; $i < scalar @tp_rates; $i++){
	my $sens = $tp_rates[$i];
	my $spe = $specificity[$i];
	$full_text = $full_text . "$sens\t$spe\n";
    }

    # TIENE EL ORDEN DE SENSITIVITY - SPECIFICITY
    text_to_file($full_text, $name);
}

sub main{

    # Extraer informacion de la tabla con puntajes
    my $aa_table = file_text("chou-fasman_table.txt");
    my $cf_ref = process_table($aa_table);
    # Mas adelante se pueden listar todos los elementos de la carpeta y hacerlo ciclicamente
    # Usar stride se puede hacer "desde fuera" del programa
    #my $stride_text = file_text("proteins/test.txt");
    my $pre_data_ref = get_real_data();
    my $data_ref = make_full_data($cf_ref, $pre_data_ref);

    my $beta_data = make_axis($data_ref, "conf_beta");
    #graphic_data($beta_data, "beta");

    my $alpha_data = make_axis($data_ref, "conf_alpha");
    #graphic_data($alpha_data, "alpha");

    my $turn_data = make_axis($data_ref, "conf_turn");
    #graphic_data($turn_data, "turn");

    make_table($alpha_data, "alphas.txt");
    make_table($beta_data, "betas.txt");
    make_table($turn_data, "turns.txt");

    # Hacer un archivo de texto a modo de tabla por cada uno de las estructuras
    
    #print_data($data_ref);
}

main();
