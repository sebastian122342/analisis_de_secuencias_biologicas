# sacar la entropia de una secuencia

import math

def entropy(text):
    na = text.count("A")
    nt = text.count("T")
    nc = text.count("C")
    ng = text.count("G")

    cants = [na, nt, nc, ng]

    ent = 0
    for cant in cants:
        prob = cant/len(text)
        ent += -prob * math.log2(prob)
    return ent

def main():

    with open("seq.txt", "r") as f:
        seq = f.readline()
    seq = seq.strip("\n")
    ent = entropy(seq)
    ent_total_bits = ent*1000
    print(f"total de la secuencia, en bits: {ent_total_bits}")
    print(f"total de la secuencia, en bytes: {ent_total_bits/8}")


if __name__ == "__main__":
    main()